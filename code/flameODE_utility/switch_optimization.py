import numpy as np
import copy
from scipy.optimize import minimize, Bounds
from flameODE_utility.model_util import LinearOscillators, build_loss_function
from flameODE_utility.general_util import DataLoader

def return_training_points(train_data, duct_u_sample):
    """
    Returns number of training points given a DataLoader object (with stored u_sample rate, 
    reduction ratio and transient phase) as well as duct_u_sample value. 
    Allows to check if enough training points are available
    """
    temp = int(np.floor(train_data['ign_val'] / duct_u_sample))
    points = len(undersample(train_data['target_signal'], duct_u_sample)[temp:])
    return points

# Create unique saving-name
def create_file_name(name):
    # Take current time to create log file name
    now = datetime.datetime.now()
    date_suffix = now.strftime('%d-%m-%Y_%H-%M-%S')
    name = name + '_' + date_suffix
    return name

def partition_data(data=DataLoader.return_training_data, partitions=20, partition_length=5):
    shift = ((data['ir_multiples'] - partition_length) / partitions * data['ir_time']) / (data['dt'] * data['u_rate'])  # Shift between partitions in data points
    shift = int(np.ceil(shift)) 
    partition_length = int(np.floor((data['ir_time'] * partition_length) / (data['dt'] * data['u_rate'])))
    # Note: Combination of ceil and floor ensure that original signal length is not exceeded 
    data_list = []
    # Loop in which newly cut data partitions are appeneded to a list
    for i in range(0, partitions):
        temp_data_dic = copy.copy(data)
        temp_data_dic['ir_multiples'] = partition_length
        temp_data_dic['input_signal'] = data['input_signal'][i*shift:i*shift + partition_length]
        temp_data_dic['target_signal'] = data['target_signal'][i*shift:i*shift + partition_length]
        data_list.append(temp_data_dic)
    return data_list

def random_init(samples: int=10, up_bound: float=[1, 1, 1, 1, 0.9, 1, 1, 1, 1], low_bound: float=[0.1, 0.1, 0.1, 0.001, 0.1, 0.1, 0.1, 0.1, 0.001], distribution='uniform', mean_values=None, variance_values=None):
    # Check User Input
    if len(up_bound) != len(low_bound):
        raise ValueError('Upper and lower bounds require same length')
    
    if distribution=='uniform':
        # Create no. of samples random initial parametrizations from 0-1    
        parameters = [random.rand(len(up_bound)) for i in range(0, samples)]
        # Scale all initial points accoridng to the provided boundaries
        for i in range(0, samples):
            parameters[i] = np.multiply(parameters[i], np.array(up_bound)-np.array(low_bound)) + np.array(low_bound)
    elif distribution=='multivariate_normal':
        # Check User Input
        if mean_values is None or variance_values is None:
            raise ValueError('Provide <mean_values> and <covariance_values> to use a multivariate normal distribution')
        if len(variance_values) != len(mean_values):
            raise ValueError('For <mean_values> with shape [k] - please provide <variance_values> of shape [k]')
        
        covariance_matrix=np.diag(variance_values)
        # Draw random samples
        parameters = np.random.multivariate_normal(mean_values, covariance_matrix, size=samples, check_valid='warn', tol=1e-8)
        
        ################ OVERKILL #############################################################
        # --> replace later with sum over entire matrix and then just redraw entire matrix... much simpler and faster
        # Check wether drawn values violate upper bounds or lower bounds at some point
        for i in range(0, samples):
            redraw_flag=True
            violating_positions=np.squeeze(np.array([]))
            while redraw_flag:
                check_array=parameters[i]-np.array(low_bound)
                violating_positions = np.append(violating_positions, np.squeeze(np.argwhere(check_array < 0.0)))
                check_array=np.array(up_bound)-parameters[i]
                violating_positions = np.append(violating_positions, np.squeeze(np.argwhere(check_array < 0.0)))
                violating_positions=np.unique(violating_positions)
                if len(violating_positions)>0:
                    print('Detected Violation')
                    for j in range(0, len(violating_positions)):
                        parameters[i, int(violating_positions[j])] = np.random.normal(mean_values[int(violating_positions[j])], variance_values[int(violating_positions[j])], 1)
                else:
                    redraw_flag=False
        ################ OVERKILL #############################################################           
    else:
        raise ValueError('Distribution: ' + distribution + ' not defined. Use <uniform> or <normal> instead')
    return parameters

def restart_from_previous_run(path):
    print('\nLoading pretrained models from file: ' + path)
    with open(path, 'rb') as f:
        data_container = pickle.load(f)
        f.close()

    data_dic = data_container.return_data()
    results = data_dic['results_dic']
    params=[]
    for i in range(0, np.shape(results['ensemble_parameter_evol'])[0]):
        params.append(results['ensemble_parameter_evol'][i][-1])
    params = np.array(params)
    return params

def switch_opt(switch_tol, max_switch, model, opti_bound, data):
    """TODO: DOCSTRING GOES HERE

    more string
    """

    TREF = model.parameters[-1]
    DUCT_USAMP = model.duct_u_sample
    init_loss_function = build_loss_function(model=model, data=data, Tref=TREF)
    curr_loss = init_loss_function(model.parameters[:-1])  # :-1 to prevent using Tref as parameter as it is provided seperately (otherwise optimizer tries to optimize it)
    parameters = np.copy(model.parameters[:-1])
    
    parameter_evol = [parameters]
    error_evol = []

    delta_error = np.inf
    counter = 0
    print('Iteration: 0 - Loss: ' + str(np.round(curr_loss, 4)) + ' - Delta Loss: [/]')
    while np.abs(delta_error) > switch_tol and counter < max_switch:
            print('Iteration: ' +  str(counter+1), end='')
            
            # First --> Optimization on LDO1
            # LDO1 with [alpha1, delta1, eps1, delay1, p_fac]
            temp_par = parameters[0:5]
            bounds = Bounds(lb=opti_bound[0, :5], ub=opti_bound[1, :5])
            full_params = np.concatenate([parameters, [TREF]])
            sup_LDO1 = LinearOscillators(model='sLDO_duct_LDO1', parameters=full_params, duct_u_sample=DUCT_USAMP)
            lin_loss_function = build_loss_function(model=sup_LDO1, data=data, Tref=TREF)
        
            result = minimize(lin_loss_function, temp_par, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                    options={'disp': False, 'maxiter': 1500})
            
            parameters[0:5] = result.x

            # Second --> Optimization on LDO2 
            # LDO2 with [p_fac, alpha2, delta2, eps2, delay2]
            temp_par = parameters[4:]
            bounds = Bounds(lb=opti_bound[0, 4:], ub=opti_bound[1, 4:])
            full_params = np.concatenate([parameters, [TREF]])
            sup_LDO2 = LinearOscillators(model='superimposed_mod2_LDO_duct_LDO2', parameters=full_params, duct_u_sample=DUCT_USAMP)
            lin_loss_function = build_loss_function(model=sup_LDO2, data=data, Tref=TREF)
            
            result = minimize(lin_loss_function, temp_par, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                    options={'disp': False, 'maxiter': 1500})
            
            parameters[4:] = result.x
            loss_2 = result.fun # --> Loss after optimization of LDO2
            
            parameter_evol.append(np.copy(parameters))
            error_evol.append(np.copy(loss_2))
    
            # Calculate delta J w.r.t to loss before iteration step!
            delta_error = curr_loss-loss_2
            print( ' - Loss: ' + str(np.round(loss_2, 4)) + ' - Delta Loss: ' + str(np.round(delta_error, 6)))
            curr_loss = loss_2
            counter = counter+1
    return parameter_evol, error_evol

def ensemble_switch_trainer(data_segments, switch_tol, max_switch, model, opti_bound):
    """TODO: DOCSTRING GOES HERE

    more string
    """
    ensemble_parameter_evol=[]
    ensemble_error_evol=[]
    partitions = len(data_segments)
    # Loop over all ensemble members
    for i in range(0,partitions):
        print('\n##############################################################')
        print('Calibrating Ensemble Member: ' + str(i+1) + ' of '  + str(len(data_segments)))
        print('##############################################################\n')

        train_data = data_segments[i]
        # Run switch optimization on current partition of data --> returns parameter and error evolution
        parameter_evol, error_evol = switch_opt(switch_tol, max_switch, model[i], opti_bound, data=train_data)
        ensemble_parameter_evol.append(parameter_evol)
        ensemble_error_evol.append(error_evol)

    return ensemble_parameter_evol, error_evol
