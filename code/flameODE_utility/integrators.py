import numpy as np
from flameODE_utility.general_util import add_diagonal

####################################################################################################
                                            # Integrators
####################################################################################################

# Temporal Integrators
# TODO: Build adaptive RK4 integrator
def build_linear_integrator(sys_matrix, input_vec, dt, scheme='RK4'):
    """
    Builds an integrator scheme for linear models based on a system matrix and a input vector.
    Available modes:
    'Explicit Euler' https://en.wikipedia.org/wiki/Euler_method
    'RK4' https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods
    'Trapezoidal' https://en.wikipedia.org/wiki/Trapezoidal_rule_(differential_equations)
    """
    
    if scheme=='Explicit Euler':
        def integrator(x, input_sig):
            # Schemes provide 2 inputs as default - need only first for simple explicit euler
            y = x + dt * (sys_matrix.dot(x) + input_vec * input_sig[0])
            return y

    elif scheme=='RK4':
        def integrator(x, input_sig):
            if len(input_sig) != 2:
                raise ValueError('RK4 Solver requires input signal at discrete step n, n+1 to produce ouput for step n')
            k1 = sys_matrix.dot(x) + input_vec * input_sig[0]
            k2 = sys_matrix.dot(x + dt / 2 * k1) + input_vec * (input_sig[0] + input_sig[1]) / 2
            k3 = sys_matrix.dot(x + dt / 2 * k2) + input_vec * (input_sig[0] + input_sig[1]) / 2
            k4 = sys_matrix.dot(x + dt * k3) + input_vec * input_sig[1]

            y = x + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4) 
            return y

    elif scheme=='Trapezoidal':
        dim = np.shape(sys_matrix)[0]
        def integrator(x, input_sig):
            if len(input_sig) != 2:
                raise ValueError('Trapezoidal Solver requires input signal at discrete step n, n+1 to produce ouput for step n')
            b = x + dt/2 * (input_vec * (input_sig[0] + input_sig[1]) + sys_matrix.dot(x))
            y = np.linalg.solve(np.eye(dim) - dt/2 * sys_matrix, b)
            return y

    else:
        raise ValueError('Scheme ' + scheme + ' not recognized - Please use Explict Euler, RK4 or Trapezoidal instead')
    return integrator

def build_non_linear_integrator(sys_matrix, input_vec, nonlin_func, dt, scheme='RK4'):
    """
    Builds an integrator scheme for non-linear models based on a system matrix and a input vector.
    Available modes:
    'Explicit Euler' https://en.wikipedia.org/wiki/Euler_method
    'RK4' https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods
    'Trapezoidal' https://en.wikipedia.org/wiki/Trapezoidal_rule_(differential_equations)
    The linear part of the system as well as the input vector are modelled/treated as in the linear case, where as 
    the nonlinear part of the system has to be provided as a function with:
    z_dot = f(z) 
    with state variable vector z and its time derivative z_dot. In its current implementation, the nonlinear
    part of the system cannot depend on the input. 
    """

    if scheme=='Explicit Euler':
        def integrator(x, input_sig):
            y = x + dt * (sys_matrix.dot(x) + nonlin_func(x) + input_vec * input_sig[0])
            return y

    elif scheme=='RK4':
        def integrator(x, input_sig):
            if len(input_sig) != 2:
                raise ValueError('RK4 Solver requires input signal at discrete step n, n+1 to produce ouput for step n')
            k1 = sys_matrix.dot(x) + nonlin_func(x) + input_vec * input_sig[0]
            k2 = sys_matrix.dot(x + dt / 2 * k1) + nonlin_func(x + dt / 2 * k1) + input_vec * (input_sig[0] + input_sig[1]) / 2
            k3 = sys_matrix.dot(x + dt / 2 * k2) + nonlin_func(x + dt / 2 * k2) + input_vec * (input_sig[0] + input_sig[1]) / 2
            k4 = sys_matrix.dot(x + dt * k3) + nonlin_func(x + dt * k3) + input_vec * input_sig[1]

            y = x + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4) 
            return y

    else:
        raise ValueError('Scheme ' + scheme + ' not recognized - Please use Explict Euler or RK4 instead')
    return integrator

# Spatial Integrators for duct
def build_duct_approximation(duct_length=1, dx=0.01, c=1, order=4, Tref=1, Lref=1):
    """
    Coefficients for approximation of spatial gradients taken from:
    https://en.wikipedia.org/wiki/Finite_difference_coefficient
    https://web.media.mit.edu/~crtaylor/calculator.html
    """
    steps = int(np.ceil(duct_length/dx))
    input_vec = np.zeros(steps)

    if order == 1:
        # Build system matrix
        sys_matrix = np.zeros([steps, steps])
        sys_matrix = add_diagonal(sys_matrix, k=0, value=1)
        sys_matrix = add_diagonal(sys_matrix, k=-1, value=-1)

        # Build input vector - scaling input according to scheme order to impose forcing at duct inlet
        input_vec[0] = -1

    elif order == 2:
        # Build system matrix
        sys_matrix = np.eye(steps) * 3 / 2
        sys_matrix = add_diagonal(sys_matrix, -1, -2)
        sys_matrix = add_diagonal(sys_matrix, -2, 1 / 2)

        # First lines of sys_matrix need diff. coefficients as start up process is of lower order
        sys_matrix[0, 0] = 1

        # Build input vector - scaling input according to scheme order to impose forcing at duct inlet
        input_vec[0] = -1
        input_vec[1] = 1 / 2

    elif order == 3:
        # Build system matrix
        sys_matrix = np.eye(steps) * 11 / 6
        sys_matrix = add_diagonal(sys_matrix, -1, -3)
        sys_matrix = add_diagonal(sys_matrix, -2, 3/2)
        sys_matrix = add_diagonal(sys_matrix, -3, -1/3)

        # First lines of sys_matrix need diff. coefficients as start up process is of lower order
        sys_matrix[0, 0] = 1
        sys_matrix[1, 0] = -2
        sys_matrix[1, 1] = 1.5
        sys_matrix[2, 0] = 1.5
        sys_matrix[2, 1] = -3.0
        sys_matrix[2, 2] = 11 / 6

        # Build input vector - scaling input according to scheme order to impose forcing at duct inlet
        input_vec[0] = -1
        input_vec[1] = 0.5
        input_vec[3] = -1/3

    elif order == 4:
        # Build system matrix
        sys_matrix = np.zeros([steps, steps])
        sys_matrix = add_diagonal(sys_matrix, k=0, value=25/12)
        sys_matrix = add_diagonal(sys_matrix, k=-1, value=-4)
        sys_matrix = add_diagonal(sys_matrix, k=-2, value=3)
        sys_matrix = add_diagonal(sys_matrix, k=-3, value=-4/3)
        sys_matrix = add_diagonal(sys_matrix, k=-4, value=1/4)

        # First lines of sys_matrix need diff. coefficients as start up process is of lower order
        sys_matrix[0, 0] = 1
        sys_matrix[1, 0] = -2
        sys_matrix[1, 1] = 3/2
        sys_matrix[2, 0] = 3/2
        sys_matrix[2, 1] = -3
        sys_matrix[2, 2] = 11/6
        sys_matrix[3, 0] = -4/3
        sys_matrix[3, 1] = 3
        sys_matrix[3, 2] = -4
        sys_matrix[3, 3] = 25/12

        # Build input vector - scaling input according to scheme order to impose forcing at duct inlet
        input_vec[0] = -1
        input_vec[1] = 1/2
        input_vec[2] = -1/3
        input_vec[3] = 1/4
    else:
        raise ValueError('Order ' + str(order) + ' not implemented/known')
    
    # Multiply with c, dx and -1 to get correct structure for follow up time discretization
    sys_matrix = - Tref/Lref * 1 / dx * c * sys_matrix
    input_vec = - Tref/Lref * 1 / dx * c * input_vec

    return sys_matrix, input_vec

def init_duct(delay: float, dt: float, chan_l: float=1.0, dx: float=0.01, x_order: int=2, t_schene: str="Trapezoidal"):
    # TODO: Docstring
    # No. of discrete spatial points
    num_x = int(np.ceil(chan_l/dx))
    
    # Spatial Integrators
    sys_matrix, input_vector = build_duct_approximation(duct_length=chan_l, dx=dx, c=1/delay, order=x_order)

    # Temporal Integrators
    duct_integrator = build_linear_integrator(sys_matrix=sys_matrix, input_vec=input_vector, dt=dt, scheme=t_schene)
    return duct_integrator, num_x
