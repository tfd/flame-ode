import numpy as np
import os
import math
from flameODE_utility import sys_dynamics as fsd
from matplotlib import pyplot as plt
from scipy.stats import bootstrap
import h5py

def check_if_empty(dir_path):
    """
    Checks wether given directory contains at least one file (FALSE) or not (TRUE)
    """
    target_dir = os.listdir(dir_path)
    return len(target_dir)==0

# TODO: Add descriptions and def. values
def add_diagonal(matrix, k, value):

    if k >= 0:
        low_range = 0
        up_range=len(matrix)-k
    else:
        low_range=0-k
        up_range=len(matrix)

    for j in range(low_range, up_range):
        matrix[j, j+k] = value
    return matrix

def undersample(input_signal, sample_rate: int):
    sample_rate = int(sample_rate)
    # Undersamples given input signal with a given sample rate and returns undersampled signal
    samples = int(np.floor(len(input_signal)/sample_rate))
    u_sample_signal = np.zeros([samples, 1])
    for k in range(0, samples):
        u_sample_signal[k] = input_signal[k*sample_rate]
    return u_sample_signal[:, 0]

def load_data(path, u_rate:int=1):
    h5file = h5py.File(path, 'r')
    input_data = np.array(h5file.get('U'))
    output_data = np.array(h5file.get('Q'))
    ts = np.array(h5file.get('Ts'))
    h5file.close()

    # Undersample
    input_data = undersample(input_signal=input_data, sample_rate=u_rate)
    output_data = undersample(input_signal=output_data, sample_rate=u_rate)
    return input_data, output_data, ts

def cut_by_ir(input, dt, u_rate, ir_multiples: float=10.0, ir_time: float=0.015, return_end=False):
    """
    Takes a given input signal with corresponding dt and undersampling rate and cuts the signal to the length 
    of ir_multiples of the specified ir_time. Can cut data either from front or back of provided signal.
    of ir_multiples of the specified ir_time. Can cut data either from front or back of provided signal.
    Returns the cut data. 
    """
    cut_val = int(np.ceil(ir_multiples*ir_time / (dt * u_rate)))
    if cut_val > len(input):
        raise ValueError('\nData contains only ' + str(np.round(len(input)* dt * u_rate / ir_time, 2)) + " IR responses")
    if return_end:
        input  = input[len(input)-cut_val:]
    else:
        input  = input[:cut_val]
    return input

def plot_confidence_interval(x, mean, low_bound, up_bound, color='#2187bb', horizontal_line_width=2):
    # TODO: Add docstring
    left = x - horizontal_line_width / 2
    top = up_bound
    right = x + horizontal_line_width / 2
    bottom = low_bound
    plt.plot([x, x], [top, bottom], color=color, alpha=0.35, linewidth=2)
    plt.plot([left, right], [top, top], color=color, alpha=0.35)
    plt.plot([left, right], [bottom, bottom], color=color, alpha=0.35)
    plt.plot(x, mean, 'o', color=color, markeredgecolor='#000000', alpha=0.7)
    return

def fit_value(output, ref):
    """ Returns the %-fit value given a model output signal and a ref. signal
    
    Based on eq. 24 in DoehnHaer22.
    """
    fit = 100*(1-np.linalg.norm(ref-output)/(np.linalg.norm(ref-np.mean(ref))))
    return fit

# Create unique saving-name
def create_file_name(name):
    # Take current time to create log file name
    now = datetime.datetime.now()
    date_suffix = now.strftime('%d-%m-%Y_%H-%M-%S')
    name = name + '_' + date_suffix
    return name

def create_sin(amplitude: float, frequency: float, num_period: float, dt: float):
    t_period = 1 / frequency
    signal_time = np.arange(start=0, stop=num_period * t_period + dt, step=dt)
    signal = amplitude * np.sin(2 * math.pi * signal_time * frequency)
    return signal

def generate_broadband(sig_len:int, step:float, num_sin:int=10, amp_range=[-1, 1], freq_range=[1, 500]):
    """
    Produces a randomly generated broadband signal by superimposing multiple sins with randomly generated amplitudes and frequencies
    """
    signal = np.zeros(sig_len)
    time = np.arange(start=0, stop=sig_len*step, step=step)

    # Randomly draw frequency and amplitude 
    amplitude = np.random.random(num_sin) * (amp_range[1] - amp_range[0]) + amp_range[0]
    freq = np.random.random(num_sin) * (freq_range[1] - freq_range[0]) + freq_range[0]
    # Superimpose the output
    for i in range(0, num_sin):
        signal = signal + amplitude[i] * np.sin(time * 2 * np.pi * freq[i])
    return signal / num_sin

class DataLoader:
    """
    Class facilitating data management for training. It allows to set training and reference data sets, specify
    undersampling ratios, ignore_values and time steps.
    """

    def __init__(self, train_path, test_path=None, u_sample_train=15, u_sample_test=15, ign_val=0):
        self.u_rate_train = u_sample_train
        self.u_rate_test = u_sample_test
        # Load data from provided paths with given undersampling ratio
        self.training_input, self.training_target, self.dt_train = load_data(path=train_path, u_rate=u_sample_train)
        # The ignore value (transient phase that is ignored for loss calc.) is given in non-transformed time (s)
        # It is transformed in an index value which does not change during time transformation
        self.ign_val_train = int(np.ceil(ign_val / (self.dt_train * self.u_rate_train)))
        self.training_file = train_path

        # Repeat procedure if test data is given
        if test_path is None:
            self.test_input = self.test_target = self.dt_test = self.ign_val_test = None
            self.test_file = 'No file loaded'
        else:
            self.test_input, self.test_target, self.dt_test = load_data(path=test_path, u_rate=u_sample_test)
            self.ign_val_test = int(np.ceil(ign_val / (self.dt_test * self.u_rate_test)))
            self.test_file = test_path
        
    def return_training_data(self, red_ratio=None, ir_multiples: float=10.0, ir_time: float=0.015, return_end=False):
        """
        Returns training data. Length/proportion of training data can be control via either a reduction ratio 
        (e.g. red_ratio=0.5 returns 50% of all data available) or specified to be a multiple of impulse respone
        times. 
        """
        if red_ratio == None:
            # Scale by IR
            training_input = cut_by_ir(input=self.training_input, dt=self.dt_train, \
                u_rate=self.u_rate_train, ir_multiples=ir_multiples, ir_time=ir_time, return_end=return_end)
            training_target = cut_by_ir(input=self.training_target, dt=self.dt_train, \
                u_rate=self.u_rate_train, ir_multiples=ir_multiples, ir_time=ir_time, return_end=return_end)
            red_ratio = None
        else:
            training_input = self.training_input[0:int(np.ceil(len(self.training_input) * red_ratio))]
            training_target = self.training_target[0:int(np.ceil(len(self.training_target) * red_ratio))]
            ir_multiples = None
            ir_time = None

        # Returns a dictionary containing all data relevant for training of a given model
        training_dic = {'input_signal': training_input, 'target_signal': training_target, 'red_ratio': red_ratio, 
        'dt': self.dt_train, 'u_rate': self.u_rate_train, 'ign_val': self.ign_val_train, 'file': self.training_file, 
        'ir_multiples':ir_multiples, 'ir_time':ir_time}
        return training_dic
    
    def return_test_data(self, red_ratio=None, ir_multiples: float=10.0, ir_time: float=0.015, return_end=False):
        """
        Returns test data. Length/proportion of test data can be control via either a reduction ratio 
        (e.g. red_ratio=0.5 returns 50% of all data available) or specified to be a multiple of impulse respone
        times. 
        """
        if self.test_input is None:
            raise ValueError('No test data specified - Pass test data to dataloader to use this function')
        
        if red_ratio == None:
            # Scale by IR
            test_input = cut_by_ir(input=self.test_input, dt=self.dt_test, u_rate=self.u_rate_test, \
                ir_multiples=ir_multiples, ir_time=ir_time, return_end=return_end)
            test_target = cut_by_ir(input=self.test_target, dt=self.dt_test, u_rate=self.u_rate_test, \
                ir_multiples=ir_multiples, ir_time=ir_time, return_end=return_end)
            red_ratio = None
        else:
            test_input = self.test_input[0:int(np.ceil(len(self.test_input) * red_ratio))]
            test_target = self.test_target[0:int(np.ceil(len(self.test_target) * red_ratio))]
            ir_multiples = None
            ir_time = None

        # Returns a dictionary containing all data relevant for testing of a given model
        test_dic = {'input_signal': test_input, 'target_signal': test_target, 'red_ratio': red_ratio, 
        'dt': self.dt_test, 'u_rate': self.u_rate_test, 'ign_val': self.ign_val_test, 'file': self.test_file,
        'ir_multiples':ir_multiples, 'ir_time':ir_time}
        return test_dic

class SaveContainer:
    """
    Class allowing to save different kinds of data in cloudpickle format to load them later.
    """

    def __init__(self, save_path, save_name, save_dic):
        self.save_path = save_path
        self.save_name = save_name
        self.save_dic = save_dic
        return

    def save_data(self):
        if isinstance(self.save_path, str):
            os.makedirs(os.path.dirname(self.save_path), exist_ok=True)
            save_path = self.save_path + self.save_name + '.pkl'
        else:
            os.makedirs(self.save_path, exist_ok=True)
            save_path = self.save_path / self.save_name
        with open(save_path, 'wb') as pickle_file:
            pickle.dump(self, pickle_file)
        return

    def return_data(self):
        return self.save_dic

# TODO: Below requires a modified bootstrap code... How to include that in FlameODE package? 
def modified_bootstrap(quantity, n_resamples: int=200, confidence_level: float=0.95, random_state: int=1):
    # Performs boot strap on a given list of quantities of interest (e.g. a gain prediction)

    # Build dummy array with indices to draw from later
    draw_params=np.arange(0, len(quantity), step=1)
    draw_params = (draw_params,)

    # Redrawing parameters according to bootstrap via slightly modified scipy.stats bootstrap to return 
    # a list of resampled parameters - This is the only quantity used! All calculations of mean/CIs 
    # is done in subsequent code. TODO: Longterm should write on drawing code to not rely on modified 
    # scipy.stats as its bad conding practice
    _ , resampled_params = bootstrap(draw_params, np.mean, n_resamples=n_resamples, confidence_level=0.95, random_state=random_state, method='percentile')

    resampled_params = resampled_params[0] # trailing dimension

    buffer_quantity=[]
    # Loop over all resamples (simulated experiments) & taking mean of quantity of interest
    for k in range(0, n_resamples):
        buffer_quantity.append(np.mean([quantity[j] for j in resampled_params[k]], axis=0))

    # Caluclating lower and uper idx. for corresponding CIs as done in eq. 1 of ref
    #  https://www.jstor.org/stable/pdf/2530926.pdf [Buckland - 1984]
    alpha=(1-confidence_level) / 2
    # COnverting alpha to indeces - Adding minus 1 due to python convention of starting arrays at 0
    low_idx = int(np.rint((n_resamples+1)*alpha)-1)  
    high_idx = int(np.rint((n_resamples+1)*(1-alpha))-1)

    up_quantity=np.sort(buffer_quantity, axis=0)[high_idx]
    low_quantity=np.sort(buffer_quantity, axis=0)[low_idx]
    mean_quantity=np.mean(buffer_quantity, axis=0)
    return mean_quantity, up_quantity, low_quantity