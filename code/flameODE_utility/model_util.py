"""
File containing all oscillator models as classes with default functions allowing to (e.g) return parameters, plot the
impulse response, gain/phase, predict time data given an input, etc. etc.
"""

# General TODO's
# TODO: Replace arange with linspace where possible
# TODO: Replace undersample with [0::xx::... notation] --> check first if its same
# TODO: Add descriptions to all functions
# TODO: Post Processing: Misses a simple standartized plot format --> possibly look at main and copy paste sth. 
# TODO: Post Processing: Somehow add pass of train/test data fits and labels of data to model to save in save_mod function
# TODO: Build Loss Func: Add functionality that reads Tref from model instead of asking user
# TODO: Impulse Response Function: Check code - seems like a trailing 0 at the end is passed!
# TODO: Change NlinOscillator Initialization: Stupid to need to pass nlin and lin parameters seperately
# TODO: Why is dt before input signal in predict function???
# TODO: Migrate whole data management to NPZ to be able to save strings with data too for gods sake
# TODO: BodePlot numerical carries an extra dimension in gain and phase around...
# TODO: It's called phase not shift
# TODO: Replace () with parameter labels with a list
# TODO: Need to find a way to pass delay_u_sample somewhere to 
# model without destroying the interface --> possibly via init and a "set_control_parameters" function
# TODO: Replace add. parameter dictionary in bode_numerical with **kwargs and if kwargs['variable'] == None, 
# variable = variable_default
# TODO: Stupid to return data ts for undersampled signals WITHOUT acounting for undersampling 
# TODO: Replace if model == ... or model == ... with if model in [..., ....]
# TODO: Code all linear models as State Space system and use new statespace.impulse() func for all linear systems
# Numerical forcing remains an option but not the default one!

# Load required packages
import time
from xmlrpc.client import boolean
import numpy as np
import cmath
import math
import h5py
import cloudpickle as pickle
from scipy.optimize import minimize, Bounds
from scipy.stats import bootstrap
from scipy import signal
from tqdm import tqdm
import os
import pyDOE as pD
import datetime
from flameODE_utility.integrators import build_linear_integrator, build_non_linear_integrator, init_duct
from flameODE_utility.sys_dynamics import return_lin_sys_dynamics
from flameODE_utility.general_util import undersample, create_sin, fit_value
############################################################################################################
                                            # Helper Functions
############################################################################################################

def calc_gain_shift(signal, amplitude: float, frequency: float, ign_periods: float, dt: float, duct_u_sample: int=1):
    t_period = 1 / frequency
    start = int(np.ceil(ign_periods * t_period / (dt * duct_u_sample)))
    max_periods=int(np.floor(len(signal[start:])/(t_period/(dt * duct_u_sample))))
    end = int(start + np.round(np.ceil(max_periods * t_period / (dt * duct_u_sample)), 0))
    fft_signal = np.fft.fft(signal[start:end], axis=0)
    gain = abs(fft_signal) / len(fft_signal) * 2
    idx = np.argmax(gain[0:int(np.floor(len(gain) / 2)) + 1])
    gain = max(gain) / amplitude
    phase_shift = np.angle(fft_signal[idx])
    phase_shift = phase_shift + np.pi/2
    return gain, phase_shift

def calc_given_range(freq: float, amp: float, model, control_dic: dict, tref: float):
            # Solver Meta Parameters
            transient_time = control_dic['transient']
            calc_periods = control_dic['FFT_periods']
            resolution = control_dic['resolution'] * control_dic['safety_factor']

            # Loop evaluation over all input forcing amplitudes
            gain = []
            shift = []
            for a in range(0, len(amp)):
                temp_gain = np.zeros(len(freq)) 
                temp_shift = np.zeros(len(freq)) 
                print("Amplitude: " + str(amp[a]))
                time.sleep(0.1)
                for f in tqdm(range(0, len(freq))):
                    period = 1 / freq[f]
                    if freq[f] <= 5 * tref:
                        dt = period / resolution[0]
                    elif 100 * tref >= freq[f] > 5 * tref:
                        dt = period / resolution[1]
                    elif 325 * tref >= freq[f] > 100 * tref:
                        dt = period / resolution[2]
                    else:
                        dt = period / resolution[3]

                    # Calculate length of required signal based on required periods and transient time
                    transient_periods=np.ceil(transient_time / tref / period)
                    tot_periods = transient_periods+calc_periods
                    signal = create_sin(amp[a], freq[f], tot_periods, dt)

                    output = model.predict(dt*tref, signal)  # Need to scale dt by tref because model does so again during prediction

                    # Need to take additional subsampling for ducted models into account (to calc. correct transient index)
                    if model.model in ("sLDO_duct", "sLDO_duct_relaxed", "LDO_duct"):
                        temp_gain[f], temp_shift[f] = calc_gain_shift(output, amp[a], freq[f], transient_periods, dt, model.duct_u_sample)
                    else:
                        temp_gain[f], temp_shift[f] = calc_gain_shift(output, amp[a], freq[f], transient_periods, dt)

                # TODO: Needs rework --> disc is used wrongly and loop structure doesn't make sense
                if control_dic['unwrap']:
                    # Unwrap shift
                    # Check if data has unphysical jumps in order of pi
                    idx = np.where(np.diff(temp_shift) >= np.pi)[0] + 1
                    if len(idx) != 0:  # Detects sign changes in shift --> shouldn't occur
                        print("Found a non-physical jump in phase - correcting this now")
                        temp_shift = np.unwrap(temp_shift)
                        """
                        disc = np.pi*(1+control_dic['relax_increment'])
                        count = 1
                        while len(idx) != 0:
                            print("Relaxing maximum discontinuity in unwrap function to " + str(disc))
                            temp_shift = np.unwrap(temp_shift, discont=disc)
                            idx = np.where(np.diff(temp_shift) >= np.pi)[0] + 1
                            disc = disc - control_dic['relax_increment']
                            if count == control_dic['max_relaxations']:
                                print("Can't resolve discontinuous jump within user defined ranges - stopping")
                            count = count + 1
                        """
                gain.append(temp_gain)
                shift.append(temp_shift)
            return gain, shift

def delay_signal(signal, delay: float, dt: float, delay_u_sample: int=1):
    delayed_signal = np.zeros(len(signal))
    delay_steps = delay / dt
    decimal = math.modf(delay_steps)[0]
    # Shift by floored multiple of time steps
    delay_steps = int(np.floor(delay_steps))
    delayed_signal[delay_steps:] = signal[:len(signal) - delay_steps]
    delayed_signal = undersample(delayed_signal, delay_u_sample)
    inter_sig = np.zeros(len(delayed_signal))
    for i in range(0, len(delayed_signal) - 1):
        inter_sig[i] = (delayed_signal[i + 1] - delayed_signal[i]) * decimal + delayed_signal[i]
    inter_sig[-1] = (delayed_signal[-2] - delayed_signal[-3]) * decimal + delayed_signal[-2]
    delayed_signal = inter_sig
    return delayed_signal

# TODO: Delete?
def unwrap_phase(m_phase, relax_increment: float=0.005, max_relaxations: int=10):
    # Unwrap shift
    # Check if data has unphysical jumps in order of pi
    idx = np.where(np.diff(m_phase) >= np.pi)[0] + 1
    if len(idx) != 0:  # Detects sign changes in shift --> shouldn't occur
        print("Found a non-physical jump in phase - correcting this now")
        disc = 1 - relax_increment
        count = 1
        while len(idx) != 0:
            print("Relaxing maximum discontinuity in unwrap function to " + str(100 * disc) + "%")
            m_phase = np.unwrap(m_phase, discont=disc)
            idx = np.where(np.diff(m_phase) >= np.pi)[0] + 1
            disc = disc - relax_increment
            if count == max_relaxations:
                print("Can't resolve discontinuous jump within user defined ranges - stopping")
            count = count + 1
    return m_phase

def extract(lst, i: int=0):
        return list(list(zip(*lst))[i])

"""
# TODO: Already exists below?
def modified_bootstrap(params, n_resamples: int=10000, quantity=np.mean, eval_model='sLDO_duct', duct_u_sample=1, confidence_level: float=0.95, random_state: int=1, method: str='percentile'):
    # Extract T_ref
    t_ref = params[-1][-1]
    # Remove T_ref from params as it is always the same and is concatenated to result later
    params = [EN_params[:-1] for EN_params in params]
    params = (params,)
    # Redrawing parameters according to bootstrap via slightly modified scipy.stats bootstrap to return a list of resampled parameters as well
    _ , resampled_params = bootstrap(params, quantity, n_resamples=n_resamples, confidence_level=confidence_level, random_state=random_state, method=method)
    
    resampled_params = resampled_params[0]
    # Resampled parameters now have shape [no. of parameteres x no. of resamples x no. of EN-members]

    # Now calculate all quantities of interest for each EN-member, take the mean and store it
    print('\n Bootstrap UQ analysis in progress...')
    # Build storage arrays
    redrawn_gain=[]
    redrawn_phase=[]
    redrawn_IR=[]
    # Loop over all "simulated" optimization runs
    for i in tqdm(range(0, n_resamples)):
        # Build Ensemble specific storage lists
        EN_gain=[]
        EN_phase=[]
        EN_ir=[]
        # Build parameter array of shape [no. of EN-members x no. of parameters]
        EN_par = np.transpose([resampled_params[k][i] for k in range(0, np.shape(resampled_params)[0])])
        
        # Loop over EN-members
        for j in range(0, np.shape(resampled_params)[2]):
            # Build model parameters
            model_par = np.concatenate([EN_par[j], [t_ref]])
            # Build model
            model = LinearOscillators(model=eval_model, parameters=model_par, duct_u_sample=duct_u_sample)
            # Predict Bode Quantities
            bode_freq, temp_gain, temp_phase = model.bode_plot()
            EN_gain.append(np.copy(temp_gain))
            EN_phase.append(np.copy(temp_phase))
            # Predict IR Quantities
            temp_response, IR_time = model.impulse_response()
            EN_ir.append(np.copy(temp_response))
        # Take mean of quantities of interest and store it in overall ensemble results
        # EN_gain / EN_phase now has shape of [no. of resamples x no. of bode frequencies]
        redrawn_gain.append(np.mean(EN_gain, axis=0))
        redrawn_phase.append(np.mean(EN_phase, axis=0))
        redrawn_IR.append(np.mean(EN_ir, axis=0))
        
    # Create CI storage lists
    # Create storage lists for resulting CI's
    up_gain_interval_95 = []
    low_gain_interval_95 = []
    up_gain_interval_50 = []
    low_gain_interval_50 = []
    up_phase_interval_95 = []
    low_phase_interval_95 = []
    up_phase_interval_50 = []
    low_phase_interval_50 = []
    up_IR_interval_95 = []
    low_IR_interval_95 = []
    up_IR_interval_50 = []
    low_IR_interval_50 = []
    
    # Calculate mean of predicted EN-quantities
    mean_gain = np.mean(redrawn_gain, axis=0)
    mean_phase = np.mean(redrawn_phase, axis=0)
    mean_IR = np.mean(redrawn_IR, axis=0)

    # Idx. calculation as done in eq. 1 of ref https://www.jstor.org/stable/pdf/2530926.pdf [Buckland - 1984]
    alpha_95 = (1-0.95) / 2
    alpha_50 = (1-0.50) / 2
    # Adding minus one due to python convention of starting arrays at 0
    low_idx_95 = int(np.rint((n_resamples+1)*alpha_95)-1)  
    high_idx_95 = int(np.rint((n_resamples+1)*(1-alpha_95))-1)
    low_idx_50 = int(np.rint((n_resamples+1)*alpha_50)-1) 
    high_idx_50 = int(np.rint((n_resamples+1)*(1-alpha_50))-1)  

    # Sort predicted quanities of interest based on simple percentile method per frequency / time per axis
    # Bode Quantities:
    for i in range(0, len(bode_freq)):
        sorted_gain = np.sort([gain[i] for gain in redrawn_gain])
        sorted_phase = np.sort([phase[i] for phase in redrawn_phase])
        # Draw samples for CIs based on percentile method
        up_gain_interval_95.append(np.copy(sorted_gain[high_idx_95]))
        low_gain_interval_95.append(np.copy(sorted_gain[low_idx_95]))
        up_gain_interval_50.append(np.copy(sorted_gain[high_idx_50]))
        low_gain_interval_50.append(np.copy(sorted_gain[low_idx_50]))
        up_phase_interval_95.append(np.copy(sorted_phase[high_idx_95]))
        low_phase_interval_95.append(np.copy(sorted_phase[low_idx_95]))
        up_phase_interval_50.append(np.copy(sorted_phase[high_idx_50]))
        low_phase_interval_50.append(np.copy(sorted_phase[low_idx_50]))

    
    # Impulse response
    for i in range(0, len(IR_time)):
        sorted_IR = np.sort([IR[i] for IR in redrawn_IR])
        # Draw samples for CIs based on percentile method
        up_IR_interval_95.append(np.copy(sorted_IR[high_idx_95]))
        low_IR_interval_95.append(np.copy(sorted_IR[low_idx_95]))
        up_IR_interval_50.append(np.copy(sorted_IR[high_idx_50]))
        low_IR_interval_50.append(np.copy(sorted_IR[low_idx_50]))
    

    # Save results
    # Build result dictionary and return
    results={'up_gain_95': np.array(up_gain_interval_95), 'low_gain_95': np.array(low_gain_interval_95), 
             'up_gain_50': np.array(up_gain_interval_50), 'low_gain_50': np.array(low_gain_interval_50),  
             'up_phase_95': np.array(up_phase_interval_95), 'low_phase_95': np.array(low_phase_interval_95),
             'up_phase_50': np.array(up_phase_interval_50), 'low_phase_50': np.array(low_phase_interval_50),
             'mean_gain': np.array(mean_gain), 'mean_phase': np.array(mean_phase), 'bode_freq': np.array(bode_freq),
             'up_IR_95': np.array(up_IR_interval_95), 'low_IR_95': np.array(low_IR_interval_95),
             'up_IR_50': np.array(up_IR_interval_50), 'low_IR_50': np.array(low_IR_interval_50),
             'mean_IR': np.array(mean_IR), 'IR_time': np.array(IR_time)
            }
    return results
"""

def scale_ir(impulse_response, time_signal, scale=1):
    curr_scale = np.sum(impulse_response)*time_signal[1]
    response = scale / curr_scale * impulse_response
    return response

# TODO: Already exists above?
def modified_bootstrap(quantity, n_resamples: int=200, confidence_level: float=0.95, random_state: int=1):
    # Performs boot strap on a given list of quantities of interest (e.g. a gain prediction)

    # Build dummy array with indices to draw from later
    draw_params=np.arange(0, len(quantity), step=1)
    draw_params = (draw_params,)

    # Redrawing parameters according to bootstrap via slightly modified scipy.stats bootstrap to return 
    # a list of resampled parameters - This is the only quantity used! All calculations of mean/CIs 
    # is done in subsequent code. TODO: Longterm should write on drawing code to not rely on modified 
    # scipy.stats as its bad conding practice
    _ , resampled_params = bootstrap(draw_params, np.mean, n_resamples=n_resamples, confidence_level=0.95, random_state=random_state, method='percentile')

    resampled_params = resampled_params[0] # trailing dimension

    buffer_quantity=[]
    # Loop over all resamples (simulated experiments) & taking mean of quantity of interest
    for k in range(0, n_resamples):
        buffer_quantity.append(np.mean([quantity[j] for j in resampled_params[k]], axis=0))

    # Caluclating lower and uper idx. for corresponding CIs as done in eq. 1 of ref
    #  https://www.jstor.org/stable/pdf/2530926.pdf [Buckland - 1984]
    alpha=(1-confidence_level) / 2
    # COnverting alpha to indeces - Adding minus 1 due to python convention of starting arrays at 0
    low_idx = int(np.rint((n_resamples+1)*alpha)-1)  
    high_idx = int(np.rint((n_resamples+1)*(1-alpha))-1)

    up_quantity=np.sort(buffer_quantity, axis=0)[high_idx]
    low_quantity=np.sort(buffer_quantity, axis=0)[low_idx]
    mean_quantity=np.mean(buffer_quantity, axis=0)
    return mean_quantity, up_quantity, low_quantity

def build_loss_function(model, data, Tref: boolean=None, eps_constraint: boolean=False, nlin: boolean=False, mode: str='norm', integrator: str='Trapezoidal'):
        # Define the loss function for the current optimization run. Requires a data dict created
        # via a DataLoader object. 
        # Optimization of Tref can be turned off with flag "Tref_Opt" - if so, a Tref has to be 
        # stated explicitly. 
        if mode=='norm':
            if nlin:
                # Only diff. to linear loss func is model.predict_training instead of model.predict_explicit_Tref
                def loss_fun(par):
                    model_output = model.predict_training(dt=data['dt']*data['u_rate'] / Tref, input_signal=data['input_signal'], par=par, integrator=integrator)
                    if model.duct_u_sample == -1:
                        loss = np.linalg.norm(model_output[data['ign_val']:] - data['target_signal'][data['ign_val']:])
                    else:
                        temp = int(np.floor(data['ign_val'] / model.duct_u_sample))
                        loss = np.linalg.norm(model_output[temp:] - undersample(data['target_signal'], model.duct_u_sample)[temp:])
                    return loss
            else:
                if Tref == None:
                    raise ValueError("If Tref is not optimized, a Tref has to be provided to build_loss_function")

                def loss_fun(par):
                    if model.model not in {"sLDO_duct_LDO1", "sLDO_duct_LDO2"}:
                        par = np.concatenate([par, [Tref]]) # --> To comply with fact that predict_tref was removed
                    if eps_constraint:
                        par = np.concatenate([par[:2], [1], par[2:]]) # --> to comply with no. of parameters but not provide eps explicitely
                    model_output = model.predict(data['dt']*data['u_rate'], data['input_signal'], par, integrator=integrator)
                    if model.duct_u_sample == -1:
                        loss = np.linalg.norm(model_output[data['ign_val']:] - data['target_signal'][data['ign_val']:])
                    else:
                        temp = int(np.floor(data['ign_val'] / model.duct_u_sample))
                        loss = np.linalg.norm(model_output[temp:] - undersample(data['target_signal'], model.duct_u_sample)[temp:])
                    return loss
        return loss_fun

def post_process(model, train_data, test_data=None, no_dig=4, integrator='Trapezoidal'):
    # Takes a model with its parameters, two data_loader objects containing 
    # training and test data to calculate a loss and a %-fit value with the 
    # given model and target data. Results can be visualized by plotting model 
    # output vs. target output for a given time window in [s]
    
    # Quick fix below to fix duct_u_sample problem --> TODO: Code this properly and automatically
    # TODO: Post_Process lacks passing of integrator keyword // possibly best to save this all in the trainer and then load trainer state 
    # into post processing util --> same for duct_usample
    model_output = model.predict(train_data['dt']*train_data['u_rate'], train_data['input_signal'], par=model.parameters, integrator=integrator)

    if model.duct_u_sample == -1:
        train_loss = np.linalg.norm(model_output[train_data['ign_val']:] - train_data['target_signal'][train_data['ign_val']:])
        train_fit = fit_value(model_output[train_data['ign_val']:], train_data['target_signal'][train_data['ign_val']:])
    else:
        temp = int(np.floor(train_data['ign_val'] / model.duct_u_sample))
        train_loss = np.linalg.norm(model_output[temp:] - undersample(train_data['target_signal'], model.duct_u_sample)[temp:])
        train_fit = fit_value(model_output[temp:], undersample(train_data['target_signal'], model.duct_u_sample)[temp:])

    if test_data is None:
        post_res = np.round(np.array([train_loss, train_fit]), no_dig)
        model.save_post_data({'loss': train_loss, 'fit': train_fit, 'file': train_data['file']})
    else:
        # TODO: Need to add if case here for duct usample too!
        test_out = model.predict(test_data['dt']*test_data['u_rate'], test_data['input_signal'], model.parameters)
        test_loss = np.linalg.norm(test_out[test_data['ign_val']:-1] - test_data['target_signal'][test_data['ign_val']:-1])
        test_fit = fit_value(test_out[test_data['ign_val']:-1], test_data['target_signal'][test_data['ign_val']:-1])
        post_res = np.round(np.array([train_loss, train_fit, test_loss, test_fit]), no_dig)
        model.save_post_data(   {'loss': train_loss, 'fit': train_fit, 'file': train_data['file']}, 
                                {'loss': test_loss, 'fit': test_fit, 'file': test_data['file']})
    return post_res

def create_parameter_samples(samples, bounds, in_par, mode='Tref'):
    """
    Function to create a list of parameters for a given model. Sample production can range 
    from simple investigations of Tref with all other parameters remaining the same to 
    full LHS or grid search based methods. 
    """
    
    if mode =='LHS':
        # Per default mode LHS does sample Tref too!
        para = pD.lhs(len(in_par), samples, criterion='center')
        for i in range(0, len(in_par)):
            para[:, i] = para[:, i] * (bounds[1][i] - bounds[0][i]) + bounds[0][i]
        parameter_list = [para[i, :] for i in range(samples)]

    elif mode =='Tref':
        # Requires Tref bounds to be passed as Tref^-1
        # TODO: Code auto detection that switches bound order if Tref<1 detected
        t_ref_array = np.linspace(start=bounds[0], stop=bounds[1], num=samples)
        parameter_list = [np.append(in_par[:-1], 1/t_ref_array[i]) for i in range(samples)]
    return parameter_list

############################################################################################################
                                            # Class Definitions
############################################################################################################
class LinearOscillators:
    """
    Class containing all implemented linear oscillator models, bundling there key functions such as e.g. predict output
    given an input, return an impulse response, ...

    Input for initialization:
    -model: Actual model type
    -parameters: Model parameters defining the model instance
    -trained: Flag that can be set to symbolize training state of the current model
    -name: User given name for specific model. If not specified, model type is used
    -description: User given description string containing any kind of information
    -duct_u_sample: User defined undersampling of signals leaving the oscillators towards the delay inducing duct
    -n_masses: Specifies the number of masses of the n_masses model
    -eps_constraint: When activ, epsilon of any given LDO type of model is set to one per constraint (dose not work on sLDO variants)
    """

    def __init__(self, model, parameters, trained="none", name=None, description=None, duct_u_sample: int=15, integrator='RK4', n_masses=2):
        self.parameters = parameters
        self.trained = trained
        self.model = model
        self.description = description
        self.integrator = integrator
        if name is None:
            self.name = model
        else:
            self.name = name
        # Select model
        if self.model == "sLDO":
            self.parameter_labels = ("alpha1", "delta1", "epsilon1", "zeta1", "p_factor",
                                     "alpha2", "delta2", "epsilon2", "zeta2", "T_ref")
            self.no_par = 10
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "sLDO_explicit":
            self.parameter_labels = ("alpha1", "delta1", "epsilon1", "explicit_delay1", "p_factor",
                                     "alpha2", "delta2", "epsilon2", "explicit_delay2", "T_ref")
            self.no_par = 10
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "sLDO_duct" or self.model == "sLDO_duct_LDO1" or self.model == "sLDO_duct_LDO2": # Edit for switch training
            self.parameter_labels = ("alpha1", "delta1", "epsilon1", "duct_delay1", "p_factor",
                                     "alpha2", "delta2", "epsilon2", "duct_delay2", "T_ref")
            self.no_par = 10
            self.duct_u_sample = duct_u_sample
        elif self.model == "LDO":
            self.parameter_labels = ("alpha", "delta", "epsilon", "zeta", "T_ref")
            self.no_par = 5
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_duct":
            self.parameter_labels = ("alpha", "delta", "epsilon", "delay", "T_ref")
            self.no_par = 5
            self.duct_u_sample = duct_u_sample
        elif self.model == "LDO_p_fac":
            self.parameter_labels = ("alpha", "delta", "epsilon", "zeta", "p_factor", "T_ref")
            self.no_par = 6
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_p_fac_explicit":
            self.parameter_labels = ("alpha", "delta", "epsilon", "delay", "p_factor", "T_ref")
            self.no_par = 6
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_p_fac_duct":
            self.parameter_labels = ("alpha", "delta", "epsilon", "delay", "p_factor", "T_ref")
            self.no_par = 6
            self.duct_u_sample = duct_u_sample
        elif self.model == "LSO":
            self.parameter_labels = ("alpha", "zeta", "T_ref")
            self.no_par = 3
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO":
            self.parameter_labels = ("alpha", "delta", "zeta", "T_ref")
            self.no_par = 4
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LCO":
            self.parameter_labels = ("alpha", "delta", "epsilon", "zeta", "T_ref")
            self.no_par = 5
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LCO_NoTimeDelay":
            self.parameter_labels = ("alpha", "delta", "epsilon", "T_ref")
            self.no_par = 4
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_NoTimeDelay":
            self.parameter_labels = ("alpha", "delta", "T_ref")
            self.no_par = 3
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_explicit":
            self.parameter_labels = ("alpha", "delta", "epsilon", "explicit_delay", "T_ref")
            self.no_par = 5
            self.duct_u_sample = -1 # Not used in models without duct
        elif self.model == "LDO_n_masses":
            self.parameter_labels = ("alpha", "delta", "epsilon", "zeta", "gamma", "T_ref")
            self.no_par = 6
            self.duct_u_sample = -1 # Not used in models without duct
            self.n_masses = n_masses
            if self.n_masses == 1:
                print("Selected 1 Mass - Loading model LSO instead")
                self.model="LSO"
                self.parameters=[parameters[1], parameters[2], parameters[-1]]
                self.parameter_labels = ("alpha", "zeta", "T_ref")
                self.no_par = 3
            elif self.n_masses==2:
                print("Selected 2 Masses - Loading model LDO instead")
                self.model="LDO"
                self.parameters=[parameters[1], parameters[2], parameters[3], parameters[4], parameters[-1]]
                self.parameter_labels = ("alpha", "delta", "epsilon", "zeta", "T_ref")
                self.no_par = 5
        else:
            raise ValueError('Model ' + self.model + " is not implemented")
        # Check provided parameters
        if self.no_par != len(self.parameters):
            raise ValueError('Parameters do not match expected length. \nExpected: '
                             + str(self.no_par) + " \nUser input: " + str(len(self.parameters)))

    def predict(self, dt, input_signal, par=None, integrator=None):
        if integrator is None:
            integrator = self.integrator

        # -------------------------- Model: Different versions of sLDO model type ------------------------------
        if self.model in {"sLDO", "sLDO_explicit", "sLDO_duct", "sLDO_duct_LDO1", "sLDO_duct_LDO2"}:

            if par is None:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model,
                                                                                par=self.parameters)
            else:
                if self.model == "sLDO_duct_LDO1":
                    # Switch-Optimization requires diff. treatment of input parameters
                    par=np.concatenate([par, self.parameters[5:]])
                elif self.model=="sLDO_duct_LDO2":
                    par=np.concatenate([self.parameters[:4], par, [self.parameters[-1]]])
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model, par=par)
            dt = dt / par[-1]

            # Span state space
            z = np.zeros((np.shape(up_matrix)[0], len(input_signal)))
            
            # Models using duct require initialization of duct integrator
            if self.model in {"sLDO_duct", "sLDO_duct_LDO1", "sLDO_duct_LDO2"}:
                # Storage arrays for ducted signals
                sig_duct1 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))
                sig_duct2 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))
                # Build duct integrators
                duct_integrator_1, num_x = init_duct(delay=par[3], dt=dt*self.duct_u_sample)
                duct_integrator_2, _ = init_duct(delay=par[8], dt=dt*self.duct_u_sample)
                # Duct initialised with zeros
                duct1 = np.zeros(num_x)
                duct2 = np.zeros(num_x)

            # Build Integrator and integrate sLDO in time
            lin_integrator = build_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix,
                                                    dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = lin_integrator(z[:, k], input_signal[k:k+2])
 
            # In case of duct - apply delay by integration of duct ODE
            if self.model in {"sLDO_duct", "sLDO_duct_LDO1", "sLDO_duct_LDO2"}:
                ldo1 = undersample(z[2,:], self.duct_u_sample)
                ldo2 = undersample(z[6,:], self.duct_u_sample)

                for k in range(0, len(ldo1) - 1):
                    duct1 = duct_integrator_1(duct1, input_sig=ldo1[k:k+2])
                    sig_duct1[k] = duct1[-1]
                    duct2 = duct_integrator_2(duct2, input_sig=ldo2[k:k+2])
                    sig_duct2[k] = duct2[-1]

                out = sig_duct1 + sig_duct2
            elif self.model == "sLDO_explicit":
                # Apply simple signal shift --> explicit delay
                out = delay_signal(np.transpose(z[2, :]), par[3], dt, 1) + delay_signal(np.transpose(z[6, :]), par[8], dt, 1)
            else:
                # Superimpose the output that used a Padé delay
                out = np.transpose(z[4, :]) + np.transpose(z[9, :])

        # -------------------------- Model: Different versions of LDO model type --------------------------------
        elif self.model in {"LDO", "LDO_p_fac", "LDO_explicit", "LDO_duct", "LDO_p_fac_explicit", "LDO_p_fac_duct"}:
            if par is None:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model,
                                                                                par=self.parameters)
            else:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model, par=par)
            dt = dt / par[-1]

            # Span state space
            z = np.zeros((np.shape(up_matrix)[0], len(input_signal)))

            # Models using duct require initialization of duct integrator
            if self.model in {"LDO_duct", "LDO_p_fac_duct"}:
                # Storage arrays for ducted signals
                sig_duct = np.zeros(len(undersample(input_signal, self.duct_u_sample)))
                
                # Build duct integrators
                duct_integrator, num_x = init_duct(delay=par[3], dt=dt*self.duct_u_sample)
                
                # Duct initialised with zeros
                duct = np.zeros(num_x)
            
            # Build Integrator and integrate LDO in time
            lin_integrator = build_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = lin_integrator(z[:, k], input_signal[k:k+2])

            # In case of duct - apply delay by integration of duct ODE
            if self.model in {"LDO_duct", "LDO_p_fac_duct"}:
                ldo = undersample(z[2,:], self.duct_u_sample)

                for k in range(0, len(ldo) - 1):
                    duct = duct_integrator(duct, input_sig=ldo[k:k+2])
                    sig_duct[k] = duct[-1]
                out = sig_duct

            elif self.model in {"LDO_explicit", "LDO_p_fac_explicit"}:
                # Apply simple signal shift --> explicit delay
                out = delay_signal(np.transpose(z[2, :]), par[3], dt, 1)
            else:
                # Superimpose the output that used a Padé delay
                out = np.transpose(z[4, :])

        # -------------------------- Model: Special LDO with variable no. of masses --------------------------------
        elif self.model == "LDO_n_masses":
            if par is None:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model,
                                                                              par=[self.parameters, self.n_masses])
            else:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model,
                                                                              par=[par, self.n_masses])
            dt = dt / par[0][-1]

            # Span state space
            z = np.zeros((np.shape(up_matrix)[0], len(input_signal)))

            # Build Integrator and integrate LDO-type model in time
            lin_integrator = build_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = lin_integrator(z[:, k], input_signal[k:k+2])
            # Return Output
            out = np.transpose(z[self.n_masses*2, :])

        # ------------------------------------- Model: LSO Model -----------------------------------------------
        elif self.model == "LSO":

            if par is None:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model,
                                                                                par=self.parameters)
            else:
                up_matrix, input_signal_matrix, _, par = return_lin_sys_dynamics(model=self.model, par=par)
            dt = dt / par[-1]

            # Span state space
            z = np.zeros((np.shape(up_matrix)[0], len(input_signal)))

            # Build Integrator and integrate LDO in time
            lin_integrator = build_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = lin_integrator(z[:, k], input_signal[k:k+2])
            # Return Output
            out = np.transpose(z[2, :])
        return out

    def show_parameters(self, no_digits=4):
        # Prints list with all parameters and the corresponding labels up to a specific number of digits
        print("Parameters of Model: " + self.model)
        for i in range(0, self.no_par):
            print(self.parameter_labels[i] + ": " + str(np.round(self.parameters[i], no_digits)))
        return

    def impulse_response_numerical(self, time_vec=None, duration=15, t_ref=None, dt=1e-5, transformed=True, max_dt=1e-2, auto_scale=True):
        """
        Plots the impulse response of a given model by forcing and numerical integration of governing model equations.
        If t_ref is not set, the current model_t_ref set in self.parameters will be used. 
        Returns 2 arrays. The first containing time ticks and the second containing
        the corresponding model impulse response. By default, the time transformation implied by T_ref is applied. 
        A time vector can be supplied (to e.g. compare the model IR to a given ref. IR at certain time steps). The
        time vector needs to use a const. step size throughout the system. 
        """
        # Check if a t_ref was provided and Set t_ref to parameter value of model if not specified otherwise
        if t_ref is None:
            t_ref = self.parameters[-1]

        # Create the time and input signal
        fac = None # factor measuring whether dt refinement was necessery
        if time_vec is None:
            signal_time = np.arange(start=0, stop=duration + 2*dt, step=dt)
        else: 
            dt = (time_vec[1] - time_vec[0]) / t_ref

            if dt <= max_dt:
                signal_time = time_vec / t_ref
            else:
                fac = np.ceil(dt / max_dt)
                dt = dt / fac
                signal_time = np.arange(time_vec[0] / t_ref, time_vec[-1] / t_ref + 2*dt, dt)

        input_signal = np.zeros([len(signal_time)])
        input_signal[0] = 1 / dt
        par = self.parameters.copy()
        par[-1] = 1  # Need to overwrite tref setting of model for IR! Tref is considered by axis-stretching!

        duct_flag=False
        if self.model=='sLDO_duct':
            duct_flag=True
            self.model=='sLDO_explicit'
        response = self.predict(dt, input_signal, par)
        if duct_flag:
            self.model='sLDO_duct'
        
        if transformed:
            # Convert results to physical time
            signal_time = signal_time * t_ref
            response = response / t_ref

        # Undersample if dt was changed during routine
        if fac is not None:
            signal_time = undersample(signal_time, fac)
            response = undersample(response, fac)
        
        # Account for cases with active undersampling through duct
        if self.duct_u_sample > 1:
            signal_time = undersample(signal_time, self.duct_u_sample)

         # Scale impulse response depending on the lower. freq. gain limit
        if auto_scale:
            if self.model =="sLDO_duct_relaxed":
                response = scale_ir(response, signal_time, scale=self.parameters[9])
            else:
                response = scale_ir(response, signal_time, scale=1)
 
        # Cut IR to remove trailing zero
        signal_time = signal_time[:-1]
        response = response[:-1]

        # Cut IR to remove trailing zero
        signal_time = signal_time[:-1]
        response = response[:-1]

        # Save obtained data to model
        self.impulse_response_data = [signal_time, response]

        # Return time signal and model response
        return response, signal_time

    def unit_impulse_response(self, duration=15, dt=1e-2, t_ref=None, transformed=True):
        """
        Plots the impulse response of a given model based on the Lplace inveres of the analytical transfer function. 
        If t_ref is not set, the current model_t_ref set in self.parameters will be used. 
        Returns 2 arrays. The first containing time ticks and the second containing
        the corresponding model impulse response. By default, the time transformation implied by T_ref is applied. 
        A time vector can be supplied (to e.g. compare the model IR to a given ref. IR at certain time steps). The
        time vector needs to use a const. step size throughout the system. 
        # --> Duct is approximated as explicit delay!!! Mention that somewhere! To not do this, use impulse response numerical instead
        """

        # Build time signal
        signal_time = np.arange(start=0, stop=duration + 2*dt, step=dt) 

        # Check if a t_ref was provided and set t_ref to parameter value of model if not specified otherwise
        if t_ref is None:
            t_ref = self.parameters[-1]

        # sLDO models with explicit / duct delays can't be directly modelled via signal.StateSpace
        if self.model in {"sLDO_explicit", "sLDO_duct"}:
            
            # First LDO system
            up_matrix, input_vec, output_vec, par = return_lin_sys_dynamics(model="LDO_p_fac_explicit",
                                                                                    par=self.parameters[0:6])
            input_feed = np.array([0])
            dyn_sys = signal.StateSpace(up_matrix, input_vec[:, np.newaxis], output_vec[np.newaxis, :], input_feed)
            _, response_LDO1 = signal.impulse(dyn_sys, T=signal_time)
            
            # Second LDO system
            up_matrix, input_vec, output_vec, par = return_lin_sys_dynamics(model="LDO_p_fac_explicit",
                                                                            par=np.concatenate([self.parameters[5:9],
                                                                                                [1-self.parameters[4], -1]]))
            input_feed = np.array([0])
            dyn_sys = signal.StateSpace(up_matrix, input_vec[:, np.newaxis], output_vec[np.newaxis, :], input_feed)
            _, response_LDO2 = signal.impulse(dyn_sys, T=signal_time)

            # Superpose the individually delayed outputs    
            response = 0.5*delay_signal(response_LDO1, self.parameters[3], dt, 1) + 0.5*delay_signal(response_LDO2, self.parameters[8], dt, 1)

        # All other models can make use of signal.StaeSpace utilities
        else:
            up_matrix, input_vec, output_vec, par = return_lin_sys_dynamics(model=self.model,
                                                                                    par=self.parameters)
            input_feed = np.array([0])
            dyn_sys = signal.StateSpace(up_matrix, input_vec[:, np.newaxis], output_vec[np.newaxis, :], input_feed)
            _, response = signal.impulse(dyn_sys, T=signal_time)

            # Explicit time delay needs to be applied separately - Duct is approximated with an explicit delay!
            if self.model in {"LDO_explicit", "LDO_duct", "LDO_p_fac_explicit", "LDO_p_fac_duct"}:
                response = delay_signal(signal=response, delay=par[3], dt=dt)
        
        # Convert results to physical time domain if necessary
        if transformed:
            signal_time = signal_time * t_ref
            response = response / t_ref

        # Scale IR to obtain unit IR
        response = scale_ir(response, signal_time, scale=1)

        # Scale impulse response to obtain the unit impulse response
        response = scale_ir(response, signal_time, scale=1)
        return response, signal_time
    
    def bode_plot(self, low_lim=0, high_lim=500, step=1, unwrap=True, min_discont_tol=0.9):
        """
        Returns 3 arrays containing a frequency array [low_lim:step:high_lim] and the corresponding model gain and
        phase. If unwrap=True, the phase response is automatically unwrapped. The minimum discontinuity that will be
        detected is defined by min_discont_tol * pi
        """
        # bode(system, w=None, n=100)
        # TODO: Adding an insert here that overwrites duct models in post as explicit delay models -- 
        # WITHOUT accounting for damping effects coming from Trapezoidal and undersampled duct 
        # Later / more thourough routines will require the trainer to always produce not only a ducted model, but automatically a
        # explicit version of this model (i.e. using the same delays but retrain to get effects of duct out of model)
        
        duct_flag = False
        if self.model == "sLDO_duct":
            self.model = "sLDO_explicit"
            duct_flag = True

        if self.model == "LDO_p_fac_duct":
            self.model = "LDO_p_fac_explicit"
            duct_flag = True

        # Identify the model and select the corresponding transfer function

        # Ducted models approximate duct as explicit delay
        if self.model == "sLDO" or self.model == "sLDO_explicit" or self.model == "sdc_LDO" or self.model == "sdcc_LDO":
            def transfer_function(omega):
                # First LDO system
                alpha = self.parameters[0]
                delta = self.parameters[1]
                eps = self.parameters[2]
                zeta = self.parameters[3]
                p_fac = self.parameters[4]
                gamma = p_fac * (eps ** 2 / delta + 2 * eps) * 2

                if self.model == "sLDO":
                    term1 = 0.5 * (2 / zeta - complex(0, omega)) / (2 / zeta + complex(0, omega))
                else:
                    term1 = 0.5 * np.exp(-zeta*complex(0, omega))
                term2 = delta / (complex(0, omega) * alpha + eps + delta + complex(0,omega)**2)
                term3 = gamma / (complex(0, omega) * alpha + eps + delta + complex(0,omega)**2 -
                                 delta ** 2 / (complex(0, omega * alpha) + eps + delta + complex(0,omega)** 2))

                tf_sys_1 = term1 * term2 * term3

                # Second LDO system
                if self.model == "sdc_LDO":
                    alpha = self.parameters[0]
                    delta = self.parameters[5]
                    eps = self.parameters[6]
                    zeta = self.parameters[7]
                elif self.model == "sdcc_LDO":
                    alpha = self.parameters[0]
                    delta = self.parameters[1]
                    eps = self.parameters[4]
                    zeta = self.parameters[5]
                else:   
                    alpha = self.parameters[5]
                    delta = self.parameters[6]
                    eps = self.parameters[7]
                    zeta = self.parameters[8]
                gamma = (1 - p_fac) * (eps ** 2 / delta + 2 * eps) * 2

                if self.model == "sLDO":
                    term1 = 0.5 * (2 / zeta - complex(0, omega)) / (2 / zeta + complex(0, omega))
                else:
                    term1 = 0.5 * np.exp(-zeta*complex(0, omega))
                term2 = delta / (complex(0, omega) * alpha + eps + delta + complex(0, omega)**2)
                term3 = gamma / (complex(0, omega) * alpha + eps + delta + complex(0, omega)**2 -
                                 delta ** 2 / (complex(0, omega) * alpha + eps + delta + complex(0, omega)**2))

                tf_sys_2 = term1 * term2 * term3

                tf_sys = tf_sys_1 + tf_sys_2
                return tf_sys

        elif self.model in {"LDO_explicit", "LDO_duct", "LDO_p_fac_explicit", "LDO_NoTimeDelay"}:
            def transfer_function(omega):
                alpha = self.parameters[0]
                delta = self.parameters[1]
                eps = self.parameters[2]
                zeta = self.parameters[3]
                if self.model == "LDO_NoTimeDelay":
                    zeta =0
                
                if self.model == "LDO_p_fac_explicit":
                    gamma = (eps ** 2 / delta + 2 * eps) * self.parameters[4]
                else:    
                    gamma = (eps ** 2 / delta + 2 * eps)

                if zeta==0:
                    term1 = 1
                else:
                    term1 = np.exp(-zeta*complex(0, omega))
                term2 = delta / (complex(0, omega * alpha) + eps + delta - omega ** 2)
                term3 = gamma / (complex(0, omega * alpha) + eps + delta - omega ** 2 -
                                delta ** 2 / (complex(0, omega * alpha) + eps + delta - omega ** 2))

                tf_sys = term1 * term2 * term3
                return tf_sys

        elif self.model == "LCO" or self.model == "LCO_NoTimeDelay":
            def transfer_function(omega):
                if self.model == "LCO":
                    alpha = self.parameters[0]
                    delta = self.parameters[1]
                    eps = self.parameters[2]
                    zeta = self.parameters[3]
                    gamma = eps
                elif self.model == "LCO_NoTimeDelay":
                    alpha = self.parameters[0]
                    delta = self.parameters[1]
                    eps = self.parameters[2]
                    zeta = 0
                    gamma = eps
                
                if zeta==0:
                    term1 = 1
                else:
                    term1 = (2/zeta - complex(0,omega)) / (2/zeta + complex(0,omega))
                term2 = (alpha*complex(0,omega)+delta) / (complex(0,omega)**2 + alpha*complex(0,omega) + delta)
                term3 = (alpha*complex(0,omega)+delta) / (complex(0,omega)**2+2*alpha*complex(0,omega)+2*delta - ((alpha*complex(0,omega)+delta)**2)/(complex(0,omega)**2 + alpha*complex(0,omega) + delta))
                term4 = gamma / (complex(0,omega)**2+2*alpha*complex(0,omega)+(eps+delta) - ((alpha*complex(0,omega)+delta)**2)/(complex(0,omega)**2 + 2*alpha*complex(0,omega) + 2*delta - ((alpha*complex(0,omega)+delta)**2)/(complex(0,omega)**2 + alpha*complex(0,omega) + delta)))

                tf_sys = term1 * term2 * term3 * term4
                return tf_sys

        elif self.model == "LSO":
            def transfer_function(omega):
                alpha = self.parameters[0]
                zeta = self.parameters[1]
                gamma = 1

                term1 = term1 = (2 / zeta - complex(0, omega)) / (2 / zeta + complex(0, omega))
                term2 = gamma / (1 + complex(0, omega) * alpha + complex(0, omega)**2)

                tf_sys = term1 * term2
                return tf_sys

        else:
            raise ValueError('Function bode_plot() not implemented for model ' + self.model)

        freq_save = np.arange(start=low_lim, stop=high_lim, step=step)
        m_gain = np.zeros(len(freq_save))
        m_phase = np.zeros(len(freq_save))

        # Convert frequencies range with corresponding model t_ref and into rad/s
        t_ref = self.parameters[-1]
        freq = freq_save * 2 * np.pi * t_ref

        for i in range(0, len(freq)):
            complex_tf = transfer_function(omega=freq[i])
            m_gain[i] = abs(complex_tf)
            m_phase[i] = cmath.phase(complex_tf)

        if unwrap:
            m_phase = np.unwrap(m_phase, discont=1)
            # Check if data still exhibits unphysical jumps in order of pi
            idx = np.where(np.diff(np.sign(m_phase)) != 0)[0] + 1
            if len(idx) != 0:  # Detects sign changes in shift --> shouldn't occur
                disc = 0.99
                while len(idx) != 0 and disc >= min_discont_tol:
                    m_phase = np.unwrap(m_phase, discont=disc)
                    idx = np.where(np.diff(np.sign(m_phase)) != 0)[0] + 1
                    disc = disc - 0.01
        
        # Save data to model
        self.bode_plot_data = [freq_save, m_gain, m_phase]
        
        # TODO: REMOVE LATER
        if duct_flag:
            if self.model == "sLDO_explicit":
                self.model = "sLDO_duct"
            if self.model == "LDO_p_fac_explicit":
                self.model = "LDO_p_fac_duct"
        return freq_save, m_gain, m_phase

    def save_model(self, save_path, save_name=None):
        if save_name is None:
            save_name = self.name
        save_name = save_name + '.pkl'
        if isinstance(save_path, str):
            os.makedirs(os.path.dirname(save_path), exist_ok=True)
            save_path = save_path + save_name
        else:
            os.makedirs(save_path, exist_ok=True)
            save_path = save_path / save_name
        with open(save_path, 'wb') as pickle_file:
            pickle.dump(self, pickle_file)
        return

    def deactivate_delay(self):
        if self.model=="sLDO":
            raise ValueError('Deactivation of Delay not supported for this model') 
        elif self.model=="sLDO_explicit":
            self.parameters[3] = 0
            self.parameters[-2] = 0

        self.parameters[-2] = 0
        return

    def bode_plot_numerical(self,  forcing=[1], low_lim=1, high_lim=500, step=1, control_dic={'transient': 0.015, 'FFT_periods': 10, 
    'resolution': np.array([5000, 10000, 20000, 30000]), 'safety_factor': 2, 'unwrap': True, 'relax_increment': 0.005, 'max_relaxations': 10}):
        
        if low_lim==0:
            raise ValueError('Lower frequency limit needs to be larger than 0 for non linear models')
            
        freq = np.arange(start=low_lim, stop=high_lim+1, step=step) * self.parameters[-1]        
        gain, shift = calc_given_range(freq, forcing, self, control_dic, tref=self.parameters[-1])
        return freq / self.parameters[-1], gain[0], shift[0]

    def save_post_data(self, train_results, test_results=None):
        self.train_data = {'train_loss': train_results ['loss'], 'train_fit': train_results['fit'], 'train_file': train_results['file']}
        if test_results is not None:
            self.test_data = {'test_loss': test_results ['loss'], 'test_fit': test_results['fit'], 'test_file': test_results['file']}
        return

    def save_training_meta(self, data_loader, trainer):

        return

    def convert_to_dimensional(self):
        """ Converts a given model to its dimensional representation (i.e. in terms of mass, stiffness, ...).
        The resulting model can just be used as any other nd-model. This is achieved by setting the interally saved Tref
        to one. 
        """

        if self.model == 'sLDO_explicit':
            gamma1 = self.parameters[4] * (self.parameters[2] ** 2 / self.parameters[1] + 2 * self.parameters[2]) * 2
            gamma2 = (1 - self.parameters[4]) * (self.parameters[7] ** 2 / self.parameters[6] + 2 * self.parameters[7]) * 2
            m1 = self.parameters[-1]**2 / gamma1
            c1 = self.parameters[0]*m1 / self.parameters[-1]
            k1 = self.parameters[2]*m1 / self.parameters[-1]**2
            kc1 = self.parameters[1]*m1 / self.parameters[-1]**2
            delay1 = self.parameters[3] * self.parameters[-1]
            m2 = self.parameters[-1]**2 / gamma2
            c2 = self.parameters[5]*m2 / self.parameters[-1]
            k2 = self.parameters[7]*m2 / self.parameters[-1]**2
            kc2 = self.parameters[6]*m2 / self.parameters[-1]**2
            delay2 = self.parameters[8] * self.parameters[-1]
            self.parameters = np.array([m1, c1, k1, kc1, delay1, self.parameters[4], m2, c2, k2, kc2, delay2, 1])
            self.parameter_labels = ("Mass M1", "Damping C1", "Spring Stiffness K1", "Spring Stiffness KC1", "Explicit Delay1", "P-factor",
            "Mass M2", "Damping C2", "Spring Stiffness K1", "Spring Stiffness KC1", "Explicit Delay1", "Tref")
            self.model= 'dimensional_sLDO_explicit'
        else:
            raise ValueError('not implemented for model ' + self.model)
        return

class NonLinearOscillators:
    """
    Class containing all implemented non-linear oscillator models, bundling there 
    key functions such as e.g. predict output given an input, return an impulse response, ...

    Input for initialization:
    -parameters: Model parameters defining the model instance
    -trained: Flag that can be set to symbolize training state of the current model
    -model: Actual model type
    -name: User given name for specific model. If not specified, model type is used
    -description: User given description string containing any kind of information
    """

    def __init__(self, model, parameters, trained="none", name=None, description=None, duct_u_sample: int=15):
        self.parameters = parameters
        self.trained = trained
        self.model = model
        self.description = description
        if name is None:
            self.name = model
        else:
            self.name = name
        # Select model
        if self.model == "NLSO":
            self.decompose_parameters()
            self.parameter_labels = ("alpha", "beta", "zeta", "T_ref")
            self.no_par = 4
            self.duct_u_sample = -1
        elif self.model == "NLDO":
            self.decompose_parameters()
            self.parameter_labels = ("alpha", "beta", "delta", "zeta", "T_ref")
            self.no_par = 5
            self.duct_u_sample = -1
        elif self.model == "mod_NLDO":
            self.decompose_parameters()
            self.parameter_labels = ("alpha", "beta1", "beta2", "delta", "zeta", "T_ref")
            self.no_par = 6
            self.duct_u_sample = -1
        elif self.model == "sNLDO_duct" or self.model=="sNLDO_explicit":
            self.decompose_parameters()
            self.parameter_labels = ("alpha1", "delta1", "epsilon1", "duct_delay1", "p_factor",
                                     "alpha2", "delta2", "epsilon2", "duct_delay2", "T_ref", "beta1", "beta2")                     
            self.no_par = 12
            self.duct_u_sample = duct_u_sample
        else:
            raise ValueError('Model ' + self.model + " is not implemented")
        # Check provided parameters
        if self.no_par != len(self.parameters):
            raise ValueError('Parameters do not match expected length. \nExpected: '
                             + str(self.no_par) + " \nUser input: " + str(len(self.parameters)))
    
    def decompose_parameters(self):
        # Functionality decomposing self.parameters into linear and non-linear parameters.  
        # Needs to be executed whenever self.parameters is changed/initialized. 
        if self.model == "NLSO":
            self.lin_parameters = np.concatenate((np.array([self.parameters[0]]), self.parameters[2:]))
            self.nlin_parameters = np.array([self.parameters[1]])
        elif self.model == "NLDO":
            self.lin_parameters = np.concatenate((np.array([self.parameters[0]]), self.parameters[2:]))
            self.nlin_parameters = np.array([self.parameters[1]])
        elif self.model == "mod_NLDO":
            self.lin_parameters = np.concatenate((np.array([self.parameters[0]]), self.parameters[3:]))
            self.nlin_parameters = np.array([self.parameters[1], self.parameters[2]])
        elif self.model == "sNLDO_duct" or self.model=="sNLDO_explicit":
            self.lin_parameters = self.parameters[:-2]
            self.nlin_parameters = self.parameters[-2:]                 
        return

    def build_parameters(self):
        # Functionality constructing self.parameters from linear and non-linear parameters.  
        # Needs to be executed whenever self.linear or self.nlinear parameters are changed. 
        if self.model == "NLSO":
            self.parameters = np.concatenate(([self.lin_parameters[0]], self.nlin_parameters, self.lin_parameters[1:]))
        elif self.model == "NLDO":
            self.parameters = np.concatenate(([self.lin_parameters[0]], self.nlin_parameters, self.lin_parameters[1:]))
        elif self.model == "mod_NLDO":
            self.parameters = np.concatenate(([self.lin_parameters[0]], self.nlin_parameters, self.lin_parameters[1:]))
        elif self.model == "sNLDO_duct" or self.model=="sNLDO_explicit":
            self.parameters = np.concatenate((self.lin_parameters, self.nlin_parameters[1]))
    
    def predict_training(self, dt, input_signal, integrator='RK4', par=None):
        # Predicts using only beta as input to allow proper optimization
        if par is None:
            self.decompose_parameters()

        if self.model == "NLSO":
            # Simulates the NLSO oscillator using a pade approximation for the delay
            if par is None:
                beta = self.nlin_parameters
            else:
                beta = par
            alpha = self.lin_parameters[0]
            zeta = self.lin_parameters[1]

            z = np.zeros((3, len(input_signal)))
            up_matrix = np.array([[0, 1, 0],
                                [-1, -alpha, 0], 
                                [2/zeta, -1, -2/zeta]])
            input_signal_matrix = np.zeros(3)
            input_signal_matrix[1] = 1

            def nlin_func(x):
                y = np.zeros(3)
                y[1] = - beta * x[1] * x[0]**2
                return y

            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func = nlin_func, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])

                if zeta>0:
                    # Output Delay (via first order Padé approximation)
                    z[2, k + 1] = z[2, k] + dt * (-z[1, k] + 2 / zeta * (z[0, k] - z[2, k]))
            
            if zeta >0:
                out = np.transpose(z[2, :])
            else:
                out = np.transpose(z[0, :])

        elif self.model == "NLDO" or self.model == "mod_NLDO":
            # Simulates the LDO oscillator using a pade approximation for the delay
            if self.model=="NLDO":
                if par is None:
                    beta = self.nlin_parameters
                else:
                    beta = par
            else:
                if par is None:
                    beta1 = self.nlin_parameters[0]
                    beta2 = self.nlin_parameters[1]
                else:
                    beta1 = par[0]
                    beta2 = par[1]

            alpha = self.lin_parameters[0]
            delta = self.lin_parameters[1]
            zeta = self.lin_parameters[2]

            gamma = 2 + 1 / delta

            input_signal_matrix = np.zeros(5)
            z = np.zeros((5, len(input_signal)))
            eps = 1 
            up_matrix = np.array([[0, 1, 0, 0, 0],
                                [-(eps + delta), -alpha, delta, 0, 0],
                                [0, 0, 0, 1, 0],
                                [delta, 0, -(eps + delta), -alpha, 0],
                                [0, 0, 2 / zeta, -1, - 2 / zeta]
                                ])
            
            input_signal_matrix[1] = gamma

            if self.model=="NLDO":
                def nlin_func(x):
                    y = np.zeros(5)
                    y[1] = - beta * x[1] * x[0]**2
                    y[3] = - beta * x[3] * x[2]**2
                    return y
            else:
                def nlin_func(x):
                    y = np.zeros(5)
                    y[1] = - beta1 * x[1] * x[0]**2
                    y[3] = - beta2 * x[3] * x[2]**2
                    return y

            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func = nlin_func, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])

            out = np.transpose(z[4, :])

        elif self.model == "sNLDO_duct":
            # Non linear parameters
            if par is None:
                beta1 = self.nlin_parameters[0]
                beta2 = self.nlin_parameters[1]
            else:
                beta1 = par[0]
                beta2 = par[1]

            # Linear parameters
            alpha1, delta1, eps1, delay1, p_fac, alpha2, delta2, eps2, delay2 = self.lin_parameters[:-1]

            # Duct does not support zero delay as this would correspond to infinite speeds
            if self.model =="sNLDO_duct" and (delay1 == 0 or delay2 == 0):
                raise ValueError('Delay1 and Delay2 need to be larger then 0 for model sNLDO_duct')

            # Calc. input scaling and adjust input forcing
            gamma1 = p_fac * (eps1 ** 2 / delta1 + 2 * eps1) * 2
            gamma2 = (1 - p_fac) * (eps2 ** 2 / delta2 + 2 * eps2) * 2
            input_signal = input_signal/2
            
            # Linear state space matrices
            z = np.zeros((8, len(input_signal)))
            input_signal_matrix = np.zeros(8)
            up_matrix = np.array([[0, 1, 0, 0, 0, 0, 0, 0],
                                [-(eps1 + delta1), -alpha1, delta1, 0, 0, 0, 0, 0],
                                [0, 0, 0, 1, 0, 0, 0, 0],
                                [delta1, 0, -(eps1 + delta1), -alpha1, 0, 0, 0, 0],
                                [0, 0, 0, 0, 0, 1, 0, 0],
                                [0, 0, 0, 0, -(eps2 + delta2), -alpha2, delta2, 0],
                                [0, 0, 0, 0, 0, 0, 0, 1],
                                [0, 0, 0, 0, delta2, 0, -(eps2 + delta2), -alpha2],
                                ])

            input_signal_matrix[1] = gamma1
            input_signal_matrix[5] = gamma2

            # Non linear extension / addition
            def nlin_func(x):
                    y = np.zeros(8)
                    # First NLDO
                    y[1] = - beta1 * x[1] * x[0]**2
                    y[3] = - beta1 * x[3] * x[2]**2
                    # Second NLDO
                    y[5] = - beta2 * x[5] * x[4]**2
                    y[7] = - beta2 * x[7] * x[6]**2
                    return y
        
            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func = nlin_func, dt=dt, scheme=integrator)

            # In case of duct, spatial integrators need to be initialized 
            if self.model == "sNLDO_duct":
                chan_l = 1
                dx = 0.01
                num_x = int(np.ceil(chan_l/dx))
                
                # Duct initialised with zeros
                duct1 = np.zeros(num_x)
                duct2 = np.zeros(num_x)

                # Storage arrays for ducted signals
                sig_duct1 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))
                sig_duct2 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))

                # Spatial Integrators
                sys_matrix_02_1, input_vector_02_1 = build_duct_approximation(duct_length=chan_l, dx=dx, c=1/delay1, order=2)
                sys_matrix_02_2, input_vector_02_2 = build_duct_approximation(duct_length=chan_l, dx=dx, c=1/delay2, order=2)
                # Temporal Integrators
                duct_integrator_1 = build_linear_integrator(sys_matrix=sys_matrix_02_1, \
                    input_vec=input_vector_02_1, dt=dt*self.duct_u_sample, scheme='Trapezoidal')
                duct_integrator_2 = build_linear_integrator(sys_matrix=sys_matrix_02_2, \
                    input_vec=input_vector_02_2, dt=dt*self.duct_u_sample, scheme='Trapezoidal')

            # Loop in time for sNLDO
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])

            # Undersample output for runtime limitation
            ldo1 = undersample(z[2,:], self.duct_u_sample)
            ldo2 = undersample(z[6,:], self.duct_u_sample)

            # Send signal through ducts
            for k in range(0, len(ldo1) - 1):
                duct1 = duct_integrator_1(duct1, input_sig=ldo1[k:k+2])
                sig_duct1[k] = duct1[-1]
                duct2 = duct_integrator_2(duct2, input_sig=ldo2[k:k+2])
                sig_duct2[k] = duct2[-1]

            # Simple output superposition
            out = sig_duct1 + sig_duct2
        return out

    def predict(self, dt, input_signal, par=None, integrator='RK4'):

        if self.model == "NLSO":
            # Simulates the NLSO oscillator using a pade approximation for the delay
            if par is None:
                alpha = self.parameters[0]
                beta = self.parameters[1]
                zeta = self.parameters[2]
                dt = dt / self.parameters[-1]
            else:
                alpha = par[0]
                beta = par[1]
                zeta = par[2]
                dt = dt / par[-1]

            z = np.zeros((3, len(input_signal)))
            up_matrix = np.array([[0, 1, 0],
                                [-1, -alpha, 0], 
                                [2/zeta, -1, -2/zeta]])
            input_signal_matrix = np.zeros(3)
            input_signal_matrix[1] = 1

            def nlin_func(x):
                y = np.zeros(3)
                y[1] = - beta * x[1] * x[0]**2
                return y

            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func = nlin_func, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])
                # Output Delay (pade approx)
                z[2, k + 1] = z[2, k] + dt * (-z[1, k] + 2 / zeta * (z[0, k] - z[2, k]))

            out = np.transpose(z[2, :])

        elif self.model == "NLDO" or self.model=="mod_NLDO":
            # Simulates the LDO oscillator using a pade approximation for the delay
            if self.model=="NLDO":
                if par is None:
                    alpha = self.parameters[0]
                    beta = self.parameters[1]
                    delta = self.parameters[2]
                    zeta = self.parameters[3]
                    dt = dt / self.parameters[-1]
                else:
                    alpha = par[0]
                    beta = par[1]
                    delta = par[2]
                    zeta = par[3]
                    dt = dt / par[-1]
            else:
                if par is None:
                    alpha = self.parameters[0]
                    beta1 = self.parameters[1]
                    beta2 = self.parameters[2]
                    delta = self.parameters[3]
                    zeta = self.parameters[4]
                    dt = dt / self.parameters[-1]
                else:
                    alpha = par[0]
                    beta1 = par[1]
                    beta2 = par[2]
                    delta = par[3]
                    zeta = par[4]
                    dt = dt / par[-1]
            
            gamma = 2 + 1 / delta

            input_signal_matrix = np.zeros(5)
            z = np.zeros((5, len(input_signal)))
            eps = 1 
            up_matrix = np.array([[0, 1, 0, 0, 0],
                                [-(eps + delta), -alpha, delta, 0, 0],
                                [0, 0, 0, 1, 0],
                                [delta, 0, -(eps + delta), -alpha, 0],
                                [0, 0, 2 / zeta, -1, - 2 / zeta]
                                ])
            
            input_signal_matrix[1] = gamma

            if self.model=="NLDO":
                def nlin_func(x):
                    y = np.zeros(5)
                    y[1] = - beta * x[1] * x[0]**2
                    y[3] = - beta * x[3] * x[2]**2
                    return y
            else:
                def nlin_func(x):
                    y = np.zeros(5)
                    y[1] = - beta1 * x[1] * x[0]**2
                    y[3] = - beta2 * x[3] * x[2]**2
                    return y

            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func=nlin_func, dt=dt, scheme=integrator)
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])

            out = np.transpose(z[4, :])
        
        elif self.model == "sNLDO_duct" or self.model=="sNLDO_explicit":
            # Parameters
            if par is None:
                alpha1, delta1, eps1, delay1, p_fac, alpha2, delta2, eps2, delay2, t_ref, beta1, beta2 = self.parameters
            else:
                alpha1, delta1, eps1, delay1, p_fac, alpha2, delta2, eps2, delay2, t_ref, beta1, beta2 = par
            dt = dt / t_ref

            # Duct does not support zero delay as this would correspond to infinite speeds
            if self.model =="sNLDO_duct" and (delay1 == 0 or delay2 == 0):
                raise ValueError('Delay1 and Delay2 need to be larger then 0 for model sNLDO_duct')

            # Calc. input scaling and adjust input forcing
            gamma1 = p_fac * (eps1 ** 2 / delta1 + 2 * eps1) * 2
            gamma2 = (1 - p_fac) * (eps2 ** 2 / delta2 + 2 * eps2) * 2
            input_signal = input_signal/2
            
            # Linear state space matrices
            z = np.zeros((8, len(input_signal)))
            input_signal_matrix = np.zeros(8)
            up_matrix = np.array([[0, 1, 0, 0, 0, 0, 0, 0],
                                [-(eps1 + delta1), -alpha1, delta1, 0, 0, 0, 0, 0],
                                [0, 0, 0, 1, 0, 0, 0, 0],
                                [delta1, 0, -(eps1 + delta1), -alpha1, 0, 0, 0, 0],
                                [0, 0, 0, 0, 0, 1, 0, 0],
                                [0, 0, 0, 0, -(eps2 + delta2), -alpha2, delta2, 0],
                                [0, 0, 0, 0, 0, 0, 0, 1],
                                [0, 0, 0, 0, delta2, 0, -(eps2 + delta2), -alpha2],
                                ])

            input_signal_matrix[1] = gamma1
            input_signal_matrix[5] = gamma2

            # Non linear extension / addition
            def nlin_func(x):
                    y = np.zeros(8)
                    # First NLDO
                    y[1] = - beta1 * x[1] * x[0]**2
                    y[3] = - beta1 * x[3] * x[2]**2
                    # Second NLDO
                    y[5] = - beta2 * x[5] * x[4]**2
                    y[7] = - beta2 * x[7] * x[6]**2
                    return y
        
            nlin_integrator = build_non_linear_integrator(sys_matrix=up_matrix, input_vec=input_signal_matrix, nonlin_func = nlin_func, dt=dt, scheme=integrator)

            # Loop in time for sNLDO
            for k in range(0, len(input_signal) - 1):
                z[:, k+1] = nlin_integrator(z[:, k], input_signal[k:k+2])
            
            # In case of duct, spatial integrators need to be initialized 
            if self.model == "sNLDO_duct":
                chan_l = 1
                dx = 0.01
                num_x = int(np.ceil(chan_l/dx))
                
                # Duct initialised with zeros
                duct1 = np.zeros(num_x)
                duct2 = np.zeros(num_x)

                # Storage arrays for ducted signals
                sig_duct1 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))
                sig_duct2 = np.zeros(len(undersample(input_signal, self.duct_u_sample)))

                # Spatial Integrators
                sys_matrix_02_1, input_vector_02_1 = build_duct_approximation(duct_length=chan_l, dx=dx, c=1/delay1, order=2)
                sys_matrix_02_2, input_vector_02_2 = build_duct_approximation(duct_length=chan_l, dx=dx, c=1/delay2, order=2)
                # Temporal Integrators
                duct_integrator_1 = build_linear_integrator(sys_matrix=sys_matrix_02_1, \
                    input_vec=input_vector_02_1, dt=dt*self.duct_u_sample, scheme='Trapezoidal')
                duct_integrator_2 = build_linear_integrator(sys_matrix=sys_matrix_02_2, \
                    input_vec=input_vector_02_2, dt=dt*self.duct_u_sample, scheme='Trapezoidal')

                # Undersample output for runtime limitation
                ldo1 = undersample(z[2,:], self.duct_u_sample)
                ldo2 = undersample(z[6,:], self.duct_u_sample)

                # Send signal through ducts
                for k in range(0, len(ldo1) - 1):
                    duct1 = duct_integrator_1(duct1, input_sig=ldo1[k:k+2])
                    sig_duct1[k] = duct1[-1]
                    duct2 = duct_integrator_2(duct2, input_sig=ldo2[k:k+2])
                    sig_duct2[k] = duct2[-1]
            
            else:   
                    # Model uses explicit delay instead of duct
                    sig_duct1=delay_signal(z[2,:], delay1, dt)
                    sig_duct2=delay_signal(z[6,:], delay2, dt)
            # Simple output superposition
            out = sig_duct1 + sig_duct2
        return out

    def show_parameters(self, no_digits=4):
        # Prints list with all parameters and the corresponding labels up to a specific number of digits
        print("Parameters of Model: " + self.model)
        for i in range(0, self.no_par):
            print(self.parameter_labels[i] + ": " + str(np.round(self.parameters[i], no_digits)))
        return

    def impulse_response(self, duration=15, t_ref=None, dt=1e-5, transformed=True):
        """
        Plots the impulse response of a given model. If t_ref is not set, the current model_t_ref set
        in self.parameters will be used. Returns 2 arrays. The first containing time ticks and the second containing
        the corresponding model impulse response. By default, the time transformation implied by T_ref is applied. 
        """

        # Create the time and input signal
        signal_time = np.arange(start=0, stop=duration + dt, step=dt)
        input_signal = np.zeros([len(signal_time)])
        input_signal[0] = 1 / dt
        par = self.parameters
        par[-1] = 1  # Need to overwrite tref setting of model for IR! Tref is considered by axis-stretching!

        # Facilitate calculations by using explicit model instead of ducted model
        if self.model=="sNLDO_duct":
            duct_flag=True
            self.model="sNLDO_explicit"
            print("Approximating duct as explicit delay")
        else:
            duct_flag=False

        response = self.predict(dt, input_signal, par)

        # Set t_ref to parameter value of model (if not specified otherwise)
        if t_ref is None:
            t_ref = self.parameters[-1]

        if transformed:
            # Convert results to physical time
            signal_time = signal_time * t_ref
            response = response / t_ref

        if duct_flag:
            self.model="sNLDO_duct"

        # Return time signal and model response
        return response, signal_time

    def bode_plot(self,  forcing=[0.025, 0.25, 0.5, 1.0, 1.5], low_lim=1, high_lim=500, step=1, control_dic={'transient': 0.015, 'FFT_periods': 10, 
    'resolution': np.array([5000, 10000, 20000, 30000]), 'safety_factor': 2, 'unwrap': True, 'relax_increment': 0.005, 'max_relaxations': 10}):
        
        if low_lim==0:
            raise ValueError('Lower frequency limit needs to be larger than 0 for non linear models')

        # Facilitate calculations by using explicit model instead of ducted model
        if self.model=="sNLDO_duct":
            duct_flag=True
            self.model="sNLDO_explicit"
            print("Approximating duct as explicit delay")
        else:
            duct_flag=False
        
        freq = np.arange(start=low_lim, stop=high_lim, step=step) * self.parameters[-2]        
        gain, shift = calc_given_range(freq, forcing, self, control_dic, tref=self.parameters[-2])

        if duct_flag:
            self.model="sNLDO_duct"
            
        return freq / self.parameters[-2], gain, shift

    def save_model(self, save_path, save_name=None):
        if save_name is None:
            save_name = self.name
        save_name = save_name + '.pkl'
        if isinstance(save_path, str):
            os.makedirs(os.path.dirname(save_path), exist_ok=True)
            save_path = save_path + save_name
        else:
            os.makedirs(save_path, exist_ok=True)
            save_path = save_path / save_name
        with open(save_path, 'wb') as pickle_file:
            pickle.dump(self, pickle_file)
        return

    def deactivate_delay(self):
        self.parameters[-2] = 0
        return

    def save_post_data(self, train_results, test_results=None):
        self.train_data = {'train_loss': train_results ['loss'], 'train_fit': train_results['fit'], 'train_file': train_results['file']}
        if test_results is not None:
            self.test_data = {'test_loss': test_results ['loss'], 'test_fit': test_results['fit'], 'test_file': test_results['file']}
        return

# TODO: Implement rest of this class
class LinearTrainer:
    """
    Class designed to incorporate all training methods we use into one simple to use wrapper solution.
    """
    def __init__(self, mode, bounds=(), probe_bounds=None, samples=None, optimizer=None, save_to_model=True, disp=False, disp_min=True):
        self.mode = mode
        self.bounds = bounds
        self.probe_bounds = probe_bounds
        self.samples = samples
        self.optimizer = optimizer
        self.save_to_model = save_to_model
        self.disp = disp
        self.disp_min = disp_min

    def train(self, model, loss_fun, Tref_Opti=False, eps_constraint=False):

        if self.mode == "Gradient":
            in_guess = model.parameters

            if Tref_Opti is False:
                Tref = in_guess[-1]
                in_guess = in_guess[:-1]
            if eps_constraint:
                # Remove epsilon as it will not be optimized
                in_guess.pop(2)
            if self.bounds is not None:
                # Build bounds from lower/upper bounds provided
                bounds = Bounds(lb=self.bounds[0, :], ub=self.bounds[1, :])
            else:
                bounds = None
            if self.disp_min:
                print("\nRunning Optimization")
            if self.optimizer is None:
                raise ValueError("When using Trainer in Gradient mode, an optimizer has to be specified")
            
            # Gradient Decent Training
            elif self.optimizer == 'L-BFGS-B' and self.mode=='Gradient':
                # Run optimization using the L-BFGS-B algorithm
                result = minimize(loss_fun, in_guess, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                  options={'disp': self.disp, 'maxiter': 1500}) 
                model.trained = 'L-BFGS-B'
            elif self.optimizer == 'Powell' and self.mode=='Gradient':
                # Run optimization using Powell Method
                result = minimize(loss_fun, in_guess, args=(), method='Powell', bounds=bounds, tol=None,
                                  callback=None, options={'xtol': 0.0001, 'ftol': 0.0001, 'maxiter': None,
                                                          'maxfev': None, 'disp': self.disp, 'direc': None,
                                                          'return_all': False})
                model.trained = 'Powell'
            else:
                raise ValueError("Optimizer " + self.optimizer + " is not supported")
            if self.disp_min:
                print("\nFinished Optimization")

        if self.save_to_model:
            model.parameters = result.x
            if Tref_Opti == False:
                model.parameters = np.append(result.x, Tref)
            if eps_constraint:
                # Reinsert constraint epsilon value of 1
                model.parameters = np.concatenate([model.parameters[:2], [1], model.parameters[2:]])
       
        return result

class NonLinearTrainer:
    """
    Class designed to incorporate all training methods we use into one simple to use wrapper solution.
    """
    def __init__(self, mode, bounds=None, probe_bounds=(), samples=None, optimizer=None, save_to_model=True):
        self.mode = mode
        self.bounds = bounds
        self.probe_bounds = probe_bounds
        self.samples = samples
        self.optimizer = optimizer
        self.save_to_model = save_to_model

    def train(self, model, loss_fun, Tref=1):

        if self.mode == "Gradient":
            in_guess = model.nlin_parameters

            if self.bounds is not None:
                # Build bounds from lower/upper bounds provided
                bounds = Bounds(lb=self.bounds[0, :], ub=self.bounds[1, :])
            else:
                bounds = None

            print("\nRunning Optimization")
            if self.optimizer is None:
                raise ValueError("When using Trainer in Gradient mode, an optimizer has to be specified")
            elif self.optimizer == 'L-BFGS-B':
                # Run optimization using the L-BFGS-B algorithm
                result = minimize(loss_fun, in_guess, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                  options={'disp': False, 'maxiter': 1500})
                model.trained = 'L-BFGS-B'
            elif self.optimizer == 'Powell':
                # Run optimization using Powell Method
                result = minimize(loss_fun, in_guess, args=(), method='Powell', bounds=bounds, tol=None,
                                  callback=None, options={'xtol': 0.0001, 'ftol': 0.0001, 'maxiter': None,
                                                          'maxfev': None, 'disp': True, 'direc': None,
                                                          'return_all': False})
                model.trained = 'Powell'
            else:
                raise ValueError("Optimizer " + self.optimizer + " is not supported")
            print("\nFinished Optimization")
        
        if self.save_to_model:
            model.nlin_parameters = result.x
            model.build_parameters()
        return result

    """
    Revised NonLinearTrainer class with different handling of (non-linear) parameters
    """
    def __init__(self, mode, bounds=None, probe_bounds=(), samples=None, optimizer=None, save_to_model=True):
        self.mode = mode
        self.bounds = bounds
        self.probe_bounds = probe_bounds
        self.samples = samples
        self.optimizer = optimizer
        self.save_to_model = save_to_model

    def train(self, model, loss_fun, Tref=1):

        if self.mode == "Gradient":
            in_guess = model.nlin_parameters

            if self.bounds is not None:
                # Build bounds from lower/upper bounds provided
                bounds = Bounds(lb=self.bounds[0, :], ub=self.bounds[1, :])
            else:
                bounds = None

            print("\nRunning Optimization")
            if self.optimizer is None:
                raise ValueError("When using Trainer in Gradient mode, an optimizer has to be specified")
            elif self.optimizer == 'L-BFGS-B':
                # Run optimization using the L-BFGS-B algorithm
                result = minimize(loss_fun, in_guess, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                  options={'disp': False, 'maxiter': 1500})
                model.trained = 'L-BFGS-B'
            elif self.optimizer == 'Powell':
                # Run optimization using Powell Method
                result = minimize(loss_fun, in_guess, args=(), method='Powell', bounds=bounds, tol=None,
                                  callback=None, options={'xtol': 0.0001, 'ftol': 0.0001, 'maxiter': None,
                                                          'maxfev': None, 'disp': True, 'direc': None,
                                                          'return_all': False})
                model.trained = 'Powell'
            else:
                raise ValueError("Optimizer " + self.optimizer + " is not supported")
            print("\nFinished Optimization")
        
        if self.save_to_model:
            model.nlin_parameters = result.x
            model.build_parameters()
        return result
