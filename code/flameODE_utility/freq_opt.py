"""
TODO Add explanation

Author: Gregor Doehner, 2024
Thermo-Fluid Dynamics Group
TUM School of Engineering and Design
Technical University of Munich
"""

# --------------------------------------------------------------------------------------------------
# Import required packages
# --------------------------------------------------------------------------------------------------
import numpy as np
import pyDOE as pD
import flameODE_utility.sys_dynamics as fsd
from scipy.optimize import minimize, Bounds

def convert_phase_gain(gain, phase):
    """ Accepts gain and phase to produce a corresponding vector of complex numbers.
    """
    # Check Inputs
    if len(gain) != len(phase):
        raise ValueError("Length of gain and phase do not match")
    
    # Span result vector and loop over data provided
    comp_vec=np.zeros(len(gain), dtype=np.complex_)
    for i in range(0, len(gain)):
        comp_vec[i]=gain[i]*np.exp(complex(0, phase[i]))
    return comp_vec

def init_freq_loss_func(ref_data, ref_freq, tref, model, loss_weight=None):
    """ Initializes a loss function in the frequency domain. 
    
    Requires ref. data [gain, phase] as row vectors, as well ascorresponding frequency resolution (Hz)
    and Tref (to convert frequencies accordingly). 
    """
    eval_freq=ref_freq * 2 * np.pi * tref
    ref_data=convert_phase_gain(gain=ref_data[:, 0], phase=ref_data[:, 1])
    
    # Was a loss weigth vector provided?
    if loss_weight is None:
        loss_weight = np.ones(len(eval_freq))
    
    if model == 'sLDO':
        def freq_loss_func(par):
            """ Loss function for optimization in frequency domain. 
            
            Accepts a set of parameters and return corresponding loss. Used as loss func for optimizer.
            """
            # Run predictions
            model_gain, model_phase = fsd.bode_sLDO(par, omega_list=eval_freq)
            model_pred = convert_phase_gain(model_gain, model_phase)
            return np.linalg.norm(np.multiply(loss_weight, (np.real(model_pred)-np.real(ref_data))))+ np.linalg.norm(np.multiply(loss_weight, np.imag(model_pred)-np.imag(ref_data)))
    else:
        def freq_loss_func(par):
            """ Loss function for optimization in frequency domain. 
            
            Accepts a set of parameters and return corresponding loss. Used as loss func for optimizer.
            """
            # Run predictions
            model_gain, model_phase = fsd.bode_LDO(par, omega_list=eval_freq)
            model_pred = convert_phase_gain(model_gain, model_phase)
            return np.linalg.norm(np.multiply(loss_weight, (np.real(model_pred)-np.real(ref_data))))+ np.linalg.norm(np.multiply(loss_weight, np.imag(model_pred)-np.imag(ref_data)))
    return freq_loss_func


def freq_opti(data: str, model: str, no_LHS:int=100000, no_LBFGS:int=1000, TREF:float=1/1000):
    """Optimize a given oscillator model on experimental FTF data in frequency domain
    
    Inputs:
    -data: Array containing columns of gain, phase, weights, frequency and rows of measurment points
    -model: Model which should be optimized
    -no_LHS: Specify number of LHS samples used
    -no_LBFGS: Specify number of samples optimized by L-BFGS-B gradient descent
    -TREF: Time transformation factor as described in Eq.(2) of Doehner et al. 2024
    """

    # ----------------------------------------------------------------------------------------------
    # Set-Up
    # ----------------------------------------------------------------------------------------------
    if  model=='sLDO':
        bounds = np.array([[0.1, 0.1, 0.1, 0.001, 0.1, 0.1, 0.1, 0.1, 0.001],
                           [10, 10, 10, 10, 0.9, 10, 10, 10, 10]])
    elif model=='LDO':
        bounds = np.array([[0.1, 0.1, 0.1, 0.001], [10, 10, 10, 10]])
    else:
        raise ValueError('Model <' + model + '> not recognized. Use <sLDO> or <LDO> instead')
    # ----------------------------------------------------------------------------------------------
    # LHS-sampling
    # ----------------------------------------------------------------------------------------------
    if model=='sLDO':
        para = pD.lhs(9, no_LHS, criterion='center')
        para[:, 0] = para[:, 0] * (bounds[1,0] - bounds[0, 0]) + bounds[0, 0]  # alpha1
        para[:, 1] = para[:, 1] * (bounds[1,1] - bounds[0, 1]) + bounds[0, 1]  # delta1
        para[:, 2] = para[:, 2] * (bounds[1,2] - bounds[0, 2]) + bounds[0, 2]  # eps1
        para[:, 3] = para[:, 3] * (bounds[1,3] - bounds[0, 3]) + bounds[0, 3]  # delay1
        para[:, 4] = para[:, 4] * (bounds[1,4] - bounds[0, 4]) + bounds[0, 4]  # p_fac
        para[:, 5] = para[:, 5] * (bounds[1,5] - bounds[0, 5]) + bounds[0, 5]  # alpha1
        para[:, 6] = para[:, 6] * (bounds[1,6] - bounds[0, 6]) + bounds[0, 6]  # delta2
        para[:, 7] = para[:, 7] * (bounds[1,7] - bounds[0, 7]) + bounds[0, 7]  # eps2
        para[:, 8] = para[:, 8] * (bounds[1,8] - bounds[0, 8]) + bounds[0, 8]  # delay2
    else:
        para = pD.lhs(4, no_LHS, criterion='center')
        para[:, 0] = para[:, 0] * (bounds[1,0] - bounds[0, 0]) + bounds[0, 0]  # alpha1
        para[:, 1] = para[:, 1] * (bounds[1,1] - bounds[0, 1]) + bounds[0, 1]  # delta1
        para[:, 2] = para[:, 2] * (bounds[1,2] - bounds[0, 2]) + bounds[0, 2]  # eps1
        para[:, 3] = para[:, 3] * (bounds[1,3] - bounds[0, 3]) + bounds[0, 3]  # delay1

    # Initialize loss function
    loss_func=init_freq_loss_func(ref_data=data[:, :2], ref_freq=data[:,3], tref=TREF, model=model,
                                  loss_weight=data[:,2])

    # Evaluate all LHS samples
    loss_vec=[loss_func(parameters) for parameters in para]

    # Build result matrix
    res = np.zeros([no_LHS, np.shape(para)[1]+1])
    res[:,0]=loss_vec
    res[:,1:]=para

    # Sort by loss
    res=res[res[:,0].argsort()]

    # ----------------------------------------------------------------------------------------------
    # L-BFGS-B optimization
    # ----------------------------------------------------------------------------------------------
    # Run gradient descent optimization for n-best samples
    bounds = Bounds(lb=bounds[0, :], ub=bounds[1, :])
    LBFGS_res = np.zeros([no_LBFGS, np.shape(res)[1]])
    for i in range(0, no_LBFGS):
        init_params = res[i, 1:]
        result = minimize(loss_func, init_params, method='L-BFGS-B', bounds=bounds, tol=1e-6,
                                    options={'disp': False, 'maxiter': 1500})
        LBFGS_res[i,0]  = result.fun
        LBFGS_res[i,1:] = result.x

    # Sort by loss and return best model
    LBFGS_res=LBFGS_res[LBFGS_res[:,0].argsort()]
    res_params = LBFGS_res[0,1:]
    res_params = np.concatenate([res_params, [TREF]])
    return res_params
