# Idea is to create a file with a single function returning the corresponding up-matrices / input vectors etc. 

import numpy as np
import cmath

# --------------------------------------------------------------------------------------------------
#                                           Time Domain Dynamics
# --------------------------------------------------------------------------------------------------
def return_lin_sys_dynamics(model, par):
    """
    TODO: Add DOCSTRING
    """
    # ------------------------------------ sLDO ------------------------------------------
    if model=="sLDO":
        alpha1, delta1, eps1, zeta1, p_fac, alpha2, delta2, eps2, zeta2, _ = par

        if zeta1 == 0 or zeta2 == 0:
            raise ValueError('Zeta1 and Zeta2 need to be larger then 0 for model sLDO')

        # Calculate input scaling and adjust input forcing
        gamma1 = p_fac * (eps1 ** 2 / delta1 + 2 * eps1)
        gamma2 = (1 - p_fac) * (eps2 ** 2 / delta2 + 2 * eps2)

        # Sys. Dynamics
        input_vec = np.zeros(10)
        output_vec = np.zeros(10)
        up_matrix = np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0, 0], 
                              [-(eps1 + delta1), -alpha1, delta1, 0, 0, 0, 0, 0, 0, 0],
                              [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                              [delta1, 0, -(eps1 + delta1), -alpha1, 0, 0, 0, 0, 0, 0],
                              [0, 0, 2 / zeta1, -1, - 2 / zeta1, 0, 0, 0, 0, 0],
                              [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                              [0, 0, 0, 0, 0, -(eps2 + delta2), -alpha2, delta2, 0, 0],
                              [0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                              [0, 0, 0, 0, 0, delta2, 0, -(eps2 + delta2), -alpha2, 0],
                              [0, 0, 0, 0, 0, 0, 0, 2 / zeta2, -1, - 2 / zeta2]
                            ])

        # Populate input and output vectors
        input_vec[1] = gamma1
        input_vec[6] = gamma2
        output_vec[4] = 1
        output_vec[9] = 1
    # ------------------------------------ sLDO duct / explicit ---------------------------
    elif model in {"sLDO_explicit", "sLDO_duct", "sLDO_duct_LDO1", "sLDO_duct_LDO2"}:

        alpha1, delta1, eps1, delay1, p_fac, alpha2, delta2, eps2, delay2, _ = par

        if model in {"sLDO_duct", "sLDO_duct_LDO1", "sLDO_duct_LDO2"} and (delay1 == 0 or delay2 == 0):
                raise ValueError('Delay1 and Delay2 need to be larger then 0 for model sLDO_duct')

        # Calculate input scaling and adjust input forcing
        gamma1 = p_fac * (eps1 ** 2 / delta1 + 2 * eps1)
        gamma2 = (1 - p_fac) * (eps2 ** 2 / delta2 + 2 * eps2)

        # Sys. Dynamics
        input_vec = np.zeros(8)
        output_vec = np.zeros(8)
        up_matrix = np.array([[0, 1, 0, 0, 0, 0, 0, 0],
                            [-(eps1 + delta1), -alpha1, delta1, 0, 0, 0, 0, 0],
                            [0, 0, 0, 1, 0, 0, 0, 0],
                            [delta1, 0, -(eps1 + delta1), -alpha1, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0, 1, 0, 0],
                            [0, 0, 0, 0, -(eps2 + delta2), -alpha2, delta2, 0],
                            [0, 0, 0, 0, 0, 0, 0, 1],
                            [0, 0, 0, 0, delta2, 0, -(eps2 + delta2), -alpha2],
                            ])
        
        # Populate input and output vectors
        input_vec[1] = gamma1
        input_vec[5] = gamma2
        output_vec[2] = 1
        output_vec[6] = 1
    # --------------------------------------- LDO / LDO_p_fac ------------------------------------
    elif model in {"LDO", "LDO_p_fac", }:
        if model=="LDO":
            alpha, delta, eps, zeta, _ = par
            p_fac = 1
        else:
            alpha, delta, eps, zeta, p_fac, _ = par
        # Calc. input scaling and adjust input forcing
        gamma = (eps ** 2 / delta + 2 * eps) * p_fac

        # Sys. Dynamics
        input_vec = np.zeros(5)
        output_vec =np.zeros(5)

        up_matrix = np.array([[0, 1, 0, 0, 0],
                            [-(eps + delta), -alpha, delta, 0, 0],
                            [0, 0, 0, 1, 0],
                            [delta, 0, -(eps + delta), -alpha, 0],
                            [0, 0, 2 / zeta, -1, - 2 / zeta]
                            ])
        
        # Populate input and output vectors
        input_vec[1] = gamma
        output_vec[2]=1

    # ---------------- LDO / LDO_p_fac models using a duct or an explicit delay ----------------
    elif model in {"LDO_explicit", "LDO_duct", "LDO_p_fac_explicit", "LDO_p_fac_duct"}:
        if model in {"LDO_explicit", "LDO_duct"}:
            alpha, delta, eps, zeta, _ = par
            p_fac = 1
        else:
            alpha, delta, eps, zeta, p_fac, _ = par
        # Calc. input scaling and adjust input forcing
        gamma = (eps ** 2 / delta + 2 * eps) * p_fac

        # Sys. Dynamics
        input_vec = np.zeros(4)
        output_vec = np.zeros(4)
        up_matrix = np.array([[0, 1, 0, 0], [-(eps + delta), -alpha, delta, 0],
                                [0, 0, 0, 1], [delta, 0, -(eps + delta), -alpha]
                                ])
        
        # Populate input and output vectors
        input_vec[1] = gamma
        output_vec[2] = 1
    
    # -------------------------- Model: Special LDO with variable no. of masses --------------------------------
    elif model == "LDO_n_masses":
        alpha, delta, _, zeta, gamma, _ = par[0]
        no_masses = par[1]

        # Sys. Dynamics which depend on number of masses
        input_vec = np.zeros(no_masses*2+1)
        output_vec = np.zeros(no_masses*2+1)

        # Build Part of Matrix modelling default LDO plus additional spring for 3rd mass
        up_matrix  = np.array([[0, 1, 0, 0],
            [-(1 + delta), -alpha, delta, 0],
            [0, 0, 0, 1],
            [delta, 0, -(1 + 2*delta), -alpha]])
        # Add missing zero columns to match dimensions
        up_matrix_LDO = np.concatenate([up_matrix, np.zeros([4, no_masses*2-3])], axis=1)

        # Build Matrix for newly added masses
        up_matrix = np.zeros([no_masses*2-4, no_masses*2+1])
        for i in range(0, no_masses*2-4):
            if i  % 2 == 0:
                up_matrix[i, i+5] = 1
            elif i==no_masses*2-4-1:
                up_matrix[i, i+1:i+5] = [delta, 0, -(1+delta), -alpha]
            else:
                up_matrix[i, i+1:i+5] = [delta, 0, -(1+2*delta), -alpha]

        # Build Pade Delay
        delay_matrix = np.zeros([1, no_masses*2+1])
        delay_matrix[0, no_masses*2-2:] = [2 / zeta, -1, - 2 / zeta]

        # Stack everything together
        up_matrix = np.concatenate([up_matrix_LDO, up_matrix, delay_matrix], axis=0)
        
        # Populate input and output vectors
        input_vec[1] = gamma
        output_vec[-1] = 1
    # ------------------------------------- Model: LSO Model -----------------------------------------------
    elif model == "LSO":
        alpha, zeta, _ = par

        # Sys. Dynamics
        input_vec = np.zeros(3)
        output_vec = np.zeros(3)
        up_matrix = np.array([[0, 1, 0],
                                [-1, -alpha, 0],
                                [2/zeta, -1, -2/zeta]])
        
        # Populate input and output vectors
        input_vec[1] = 1
        output_vec[2] = 1
    
    else:
        raise ValueError("Model <" + str(model) + "> not recognized")

    return up_matrix, input_vec, output_vec, par

# ----------------------------------------------------------------------------------------------------------
#                                         Frequency Domain Dynamics
# ----------------------------------------------------------------------------------------------------------

def bode_sLDO(par, omega_list):
    """
    Returns frequency response for a given input frequency omega (rad).
    Requires paramters par to parametrize underlying sLDO model.
    Frequency response is unwrapped and rejoined to a vector of complex numbers.
    """
    # Extract parameters and calculate input scaling
    alpha1, delta1, eps1, zeta1, p_fac, alpha2, delta2, eps2, zeta2 = par
    gamma1 = p_fac * (eps1 ** 2 / delta1 + 2 * eps1) * 2
    gamma2 = (1 - p_fac) * (eps2 ** 2 / delta2 + 2 * eps2) * 2

    # Create results vector and loop over frequencies
    gain_vec = np.zeros(len(omega_list))
    phase_vec = np.zeros(len(omega_list))
    
    for i in range(0, len(omega_list)):
        omega=omega_list[i]
        # Oscilattor LDO1
        term1 = 0.5 * np.exp(-zeta1*complex(0, omega))
        term2 = delta1 / (complex(0, omega * alpha1) + eps1 + delta1 - omega ** 2)
        term3 = gamma1 / (complex(0, omega * alpha1) + eps1 + delta1 - omega ** 2 - 
                        delta1 ** 2 / (complex(0, omega * alpha1) + eps1 + delta1 - omega ** 2))
        response1 = term1*term2*term3

        # Oscilattor LDO2
        term1 = 0.5 * np.exp(-zeta2*complex(0, omega))
        term2 = delta2 / (complex(0, omega * alpha2) + eps2 + delta2 - omega ** 2)
        term3 = gamma2 / (complex(0, omega * alpha2) + eps2 + delta2 - omega ** 2 - 
                        delta2 ** 2 / (complex(0, omega * alpha2) + eps2 + delta2 - omega ** 2))
        response2 = term1*term2*term3
    
        temp_response = response1 + response2

        gain_vec[i] = abs(temp_response)
        phase_vec[i] = cmath.phase(temp_response)

    # Unwrap phase
    phase_vec = np.unwrap(phase_vec, discont=1)

    # Rejoin to complex number
    return gain_vec, phase_vec


def bode_LDO(par, omega_list):
    """
    Returns frequency response for a given input frequency omega (rad).
    Requires paramters par to parametrize underlying LDO model.
    Frequency response is unwrapped and rejoined to a vector of complex numbers.
    """
    # Extract parameters and calculate input scaling
    alpha, delta, eps, zeta = par
    gamma = (eps ** 2 / delta + 2 * eps) * 2
    
    # Create results vector and loop over frequencies
    gain_vec = np.zeros(len(omega_list))
    phase_vec = np.zeros(len(omega_list))
    
    for i in range(0, len(omega_list)):
        omega=omega_list[i]
        # Oscilattor LDO1
        term1 = 0.5 * np.exp(-zeta*complex(0, omega))
        term2 = delta / (complex(0, omega * alpha) + eps + delta - omega ** 2)
        term3 = gamma / (complex(0, omega * alpha) + eps + delta - omega ** 2 - 
                        delta ** 2 / (complex(0, omega * alpha) + eps + delta - omega ** 2))
        temp_response = term1*term2*term3
        gain_vec[i] = abs(temp_response)
        phase_vec[i] = cmath.phase(temp_response)

    # Unwrap phase
    phase_vec = np.unwrap(phase_vec, discont=1)

    return gain_vec, phase_vec
