"""
File demonstrating how to train a given linear model in frequency domain and post process it.
As target flame we consider the Palies Combustor and use model sLDO to capture its dynamics.

Author: Gregor Doehner, 2024
Thermo-Fluid Dynamics Group
TUM School of Engineering and Design
Technical University of Munich
"""

# --------------------------------------------------------------------------------------------------
# Import required packages
# --------------------------------------------------------------------------------------------------
from pathlib import Path
import h5py
import numpy as np
from flameODE_utility.model_util import LinearOscillators
from flameODE_utility.freq_opt import freq_opti
from matplotlib import pyplot as plt
import matplotlib

if __name__ == "__main__":

    # ----------------------------------------------------------------------------------------------
    # Data Preparation
    # ----------------------------------------------------------------------------------------------

    # Define folderpaths, time normalization factor and model to use
    dir_path = Path(__file__).parent.parent.resolve()
    TREF=1/1000
    MODEL='sLDO'

    # Load data and bring it into desired form
    data_path=dir_path / "data/Palies/FDF_Palies91_FlameB.h5"
    # Load Data and bring it to correct form
    h5file = h5py.File(data_path, 'r')
    ref_gain = np.array(h5file.get('gain'))
    ref_phase = np.array(h5file.get('phase'))
    ref_freq = np.array(h5file.get('frequency'))
    h5file.close()

    # Extract data for given amplitude
    ref_gain=ref_gain[0, :]
    ref_phase = -ref_phase[0, :]

    # Set weights
    loss_weight = np.ones(len(ref_freq))
    loss_weight[4] = 2
    loss_weight[8] = 2
    loss_weight[12] = 2

    data=np.stack([ref_gain, ref_phase, loss_weight, ref_freq], axis=1)

    # ----------------------------------------------------------------------------------------------
    # Optimization call
    # ----------------------------------------------------------------------------------------------

    # Meta-Parameters for calibration
    LHS_PROBES = 100000
    LBFGSB_PROBES = 1000

    # Optimize model
    res_params = freq_opti(data=data, model=MODEL, no_LHS=LHS_PROBES, no_LBFGS=LBFGSB_PROBES, TREF=1/1000)

    # ----------------------------------------------------------------------------------------------
    # Post-Processing
    # ----------------------------------------------------------------------------------------------

    # Build a flame model using the obtained results
    MODEL = MODEL + '_explicit'
    flameModel = LinearOscillators(model=MODEL, parameters=res_params)

    # Run prediction with optimized model
    sLDO_freq, sLDO_gain, sLDO_phase = flameModel.bode_plot(high_lim=ref_freq[-1])
    sLDO_ir, sLDO_ir_time = flameModel.unit_impulse_response(duration=30)

    # Plot results and save them
    non_uni_weights=[ref_freq[4], ref_freq[8], ref_freq[12]]

    plt.figure()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(6, 3)
    plt.plot(ref_freq, ref_gain, 'k--', marker='x', markersize=3.5, label='Exerimental Data')
    plt.plot(non_uni_weights, [ref_gain[4], ref_gain[8], ref_gain[12]], linestyle='None',
                color='#DC143C', marker='x', markersize=5, label='2 Weight')
    plt.plot(sLDO_freq, sLDO_gain, '#1E90FF', label='sLDO')
    plt.legend()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.tight_layout()
    plt.savefig(dir_path / 'output/figures/Palies_Gain.png', dpi=1200)
    plt.close()

    # Phase
    plt.figure()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(6, 3)
    plt.plot(ref_freq, ref_phase, 'k--', marker='x', markersize=3.5, label='Exerimental Data')
    plt.plot(non_uni_weights, [ref_phase[4], ref_phase[8], ref_phase[12]], linestyle='None',
                color='#DC143C', marker='x', markersize=5, label='2 Weight')
    plt.plot(sLDO_freq, sLDO_phase, '#1E90FF', label='sLDO')
    plt.legend()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Phase (rad)')
    plt.tight_layout()
    plt.savefig(dir_path / 'output/figures/Palies_Phase.png', dpi=1200)
    plt.close()

    # Unit Impulse Response
    plt.figure()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(6, 3)
    plt.plot(sLDO_ir_time*1000, sLDO_ir, '#1E90FF', label='sLDO')
    plt.legend()
    plt.xlabel('Physical Time (ms)')
    plt.ylabel('Amplitude')
    plt.tight_layout()
    plt.savefig(dir_path / 'output/figures/Palies_UIR.png', dpi=1200)
    plt.close()
