"""
Script calculating the predicted FDF of a given flame model of sNLDO type. The script facilitates
calculation of gain and phase by running multiple calculations in parallel. For an explanation on
how to set the corresponding meta parameters, check out the readme file in the corresponding code
folder on the main branch.

Author: Gregor Doehner, 2024
Thermo-Fluid Dynamics Group
TUM School of Engineering and Design
Technical University of Munich
"""

# --------------------------------------------------------------------------------------------------
# Import required packages
# --------------------------------------------------------------------------------------------------
import datetime
import random
import os
import time
import h5py
import concurrent.futures
import numpy as np
from flameODE_utility.model_util import create_sin, calc_gain_shift
from flameODE_utility.model_util import NonLinearOscillators
from pathlib import Path

# --------------------------------------------------------------------------------------------------
# Set Model Parameters Here
# --------------------------------------------------------------------------------------------------

# Set Model Parameters
lin_parameters = [0.36276682, 5.72186329, 0.66689266, 2.71285873, 0.52650767, 0.42986943,
                7.19032886, 1.04578806, 1.25259313, 1/1000]
nlin_parameters = [1, 1]

# Define Frequencies and Amplitudes of Interest
forcing=[0.1, 0.5, 1.0]

t_ref = 1/1000
eval_freq = np.arange(start=0, stop=501, step=10)
eval_freq[0] = 1
eval_freq=eval_freq * t_ref

no_of_dump_points=3 # No. of bode points calculated before temporary file is dumped
no_of_workers=8     # No. of parallel threads script will use

save_name = 'sLDO_FDF_bode_results.h5'


# --------------------------------------------------------------------------------------------------
# No User Input Required Beyond This Point
# --------------------------------------------------------------------------------------------------

parameters=np.concatenate([lin_parameters, nlin_parameters])

# --------------------------------------------------------------------------------------------------
# Define function to calculate a given bode point defined by frequency and amplitde of forcing
# --------------------------------------------------------------------------------------------------
# Function needs to be defined here in order to run it in parallel in the main part of the code
def calc_bode_point(data_point):
    """TODO Add Docstring
    
    """
    tref = 1/1000
    # Meta Parameter Settings for post processing
    control_dic={'transient': 0.015, 'FFT_periods': 4, 'unwrap': True}
    freq, amp = data_point[0], data_point[1]
    print('Running: Freq=' + str(freq / tref) + ' Amp=' + str(amp))
    
    # Solver Meta Parameters
    transient_time = control_dic['transient']
    calc_periods = control_dic['FFT_periods']
    period = 1 / freq
    dt=1e-6/tref
    
    # Calculate length of required signal based on required periods and transient time
    transient_periods= np.ceil(transient_time/tref/period) # np.ceil(transient_time/tref/ period)
    tot_periods = transient_periods+calc_periods
    signal = create_sin(amp, freq, tot_periods, dt)
    parameters[9]=1
    sLDO = NonLinearOscillators(model="sNLDO_explicit", parameters=parameters)
    output = sLDO.predict(dt, input_signal=signal)

    model_gain, model_phase = calc_gain_shift(output, amp, freq, transient_periods, dt)
    res = [model_gain, model_phase, freq/tref, amp]
    # Store data in temporary file to limit memory consumtion of code
    dir_path = Path(__file__).parent.parent.resolve()
    save_path = create_file_name(dir_path / 'output/temp', random_name=True)
    hf = h5py.File(save_path, 'w')
    hf.create_dataset('res', data=res)
    hf.close()
    return

# Create unique saving-name to save intermediary files
def create_file_name(name, random_name=False, file_ending='.h5'):
    if random_name:
        # Append random number to file name
        suffix = str(random.randint(0,100000000000))
    else:
        # Take current time to create log file name
        now = datetime.datetime.now()
        suffix = now.strftime('%d-%m-%Y_%H-%M-%S')
        suffix = 'test_res' + suffix + file_ending
    name = name / suffix
    return name


def delete_all_files(dir_name):
   
    # Iterate over all the files in the directory
    for filename in os.listdir(dir_name):
        file_path = os.path.join(dir_name, filename)
        
        # Check if the path is a file and delete it
        if os.path.isfile(file_path):
           os.remove(file_path)
                

# Main Function to call 
def main():
    dir_path = Path(__file__).parent.parent.resolve()
    
    # ----------------------------------------------------------------------------------------------
    # Data Preparation
    # ----------------------------------------------------------------------------------------------

    # Build data points sorted by frequency for runtime optimization
    all_freq=eval_freq
    for i in range(1, len(forcing)):
        all_freq=np.concatenate([all_freq, eval_freq])
    all_freq=np.sort(all_freq)
    
    all_forcing=forcing
    for i in range(0, int(np.floor(len(all_freq)/len(forcing)))-1):
        all_forcing=np.concatenate([all_forcing, forcing])
    
    # Clear all files in temp folder from previous runs
    directory = dir_path / 'output/temp'
    delete_all_files(directory)

    # ----------------------------------------------------------------------------------------------
    # Run bode calculation in parallel
    # ----------------------------------------------------------------------------------------------
    all_data_points=np.stack([all_freq, all_forcing], axis=1)
    executions=int(np.ceil(len(all_data_points)/no_of_dump_points))
    # Split data in smaller chunks to avoid OOM errors
    for j in range(executions):
        if j < executions-1:
            work_load=all_data_points[j*no_of_dump_points:(j+1)*no_of_dump_points]
        else:
            work_load=all_data_points[j*no_of_dump_points:]
        
        # Inspired by: https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ProcessPoolExecutor // 06.2023
        with concurrent.futures.ProcessPoolExecutor(max_workers=no_of_workers) as executor:  # Uses xxGB of remote RAM
            executor.map(calc_bode_point, work_load)
        
    # ----------------------------------------------------------------------------------------------
    # Load all temporary storage files and obtain gain / phase
    # ----------------------------------------------------------------------------------------------

    # Load contents of folder temp and create one long array of results
    
    files = next(os.walk(directory))[2]  # directory -->  directory path as string
    num_files = len(files)
    
    for i in range(0, num_files):
        curr_file = directory / files[i]
        h5file = h5py.File(curr_file, 'r')
        res = np.array(h5file.get('res'))
        h5file.close()
        if i==0:
            res_array=res
        elif i==1:
            res_array=np.stack([res_array, res], axis=0)
        else:
            res_array=np.concatenate([res_array, [res]], axis=0)
        del res
    
    # Sort by forcing amplitude
    res_array=res_array[res_array[:, 3].argsort()] 
    # Cut results into packages corresponding to a given forcing
    res_list=[res_array[i*len(eval_freq):(i+1)*len(eval_freq), :] for i in range(0, len(forcing))]
    
    # Sort by frequency within each amplitude package
    gain_list=[]
    phase_list=[]
    freq_list=[]
    for j in range(0, len(forcing)):
        temp_gain=[res_list[j][i][0] for i in range(0, np.shape(res_list[j])[0])]
        temp_phase=[res_list[j][i][1] for i in range(0,  np.shape(res_list[j])[0])]
        temp_freq=[res_list[j][i][2] for i in range(0, np.shape(res_list[j])[0])]

        temp_gain=np.stack((temp_gain, temp_freq), axis=1)
        temp_phase=np.stack([temp_phase, temp_freq], axis=1)

        temp_gain=temp_gain[temp_gain[:, 1].argsort()]
        temp_phase=temp_phase[temp_phase[:, 1].argsort()]
        
        # Returning data as before as list of gain/phase values sorted by freq. for each forcing amplitude
        gain_list.append([val[0] for val in temp_gain])
        phase_list.append([val[0] for val in temp_phase])
        freq_list.append([val[1] for val in temp_gain])

    # Unwrapping phase data
    phase_list=[np.unwrap(phase) for phase in phase_list]

    # Save data in h5-file
    hf = h5py.File(dir_path / 'output/files' / save_name, 'w')
    hf.create_dataset('gain', data=gain_list)
    hf.create_dataset('phase', data=phase_list)
    hf.create_dataset('freq', data=freq_list)
    hf.create_dataset('forcing', data=forcing)

if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()

    print('\n Done! - Elapsed Total Time: ')
    print(str((end-start)/60) + ' minutes')
