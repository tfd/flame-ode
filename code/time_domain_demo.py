"""
File demonstrating how to train a given linear model in time domain and post process it.
As target flame we consider a laminar premixed flame in a multi-slit Bunsen burner set-up.

Author: Gregor Doehner, 2024
Thermo-Fluid Dynamics Group
TUM School of Engineering and Design
Technical University of Munich
"""

# --------------------------------------------------------------------------------------------------
# Import required packages
# --------------------------------------------------------------------------------------------------
from pathlib import Path
import numpy as np
from flameODE_utility import model_util as fmu
from flameODE_utility import general_util as fgu
from matplotlib import pyplot as plt
import matplotlib
import numpy as np 
from pathlib import Path

if __name__ == "__main__":

    # ----------------------------------------------------------------------------------------------
    # Data Preparation
    # ----------------------------------------------------------------------------------------------

    # Define folderpath
    dir_path = Path(__file__).parent.parent.resolve()

    # Linear Data
    lin_train_data_path = dir_path / 'data/SilvaEmmer15_DNS_synthetic_linear.h5'
    lin_test_data_path = dir_path / 'data/Haeringer_Linear.h5'

    # Non-linear Data  
    nlin_train_data_path = dir_path / 'data/JaensPolif17_DNS_non_linear_200ms.h5'
    nlin_test_data_path = dir_path / 'data/Haeringer_Incomp.h5'

    # Build dataloader
    lin_data = fgu.DataLoader(lin_train_data_path, lin_test_data_path, ign_val=0.0077)
    nlin_data = fgu.DataLoader(nlin_train_data_path, nlin_test_data_path, ign_val=0.0077)

    # Create trainign and test data
    lin_training_data = lin_data.return_training_data(ir_multiples=6.5)
    lin_test_data = lin_data.return_test_data()
    nlin_training_data = nlin_data.return_training_data()
    nlin_test_data = nlin_data.return_test_data()

    # ----------------------------------------------------------------------------------------------
    # Model Set-Up
    # ----------------------------------------------------------------------------------------------
    MODEL='LDO'
    T_ref = 1 / 833.34
    in_par = np.array([1, 1, 1, 1, T_ref])
    bound = np.array([[0.1, 0.1, 0.1, 0.1], [5, 5, 5, 1.5]])  

    lin_model = fmu.LinearOscillators(model=MODEL, parameters=in_par)

    # ----------------------------------------------------------------------------------------------
    # Loss Function and Optimizer
    # ----------------------------------------------------------------------------------------------

    # Build loss function and deactivate optimization of Tref
    lin_loss_function = fmu.build_loss_function(model=lin_model, data=lin_training_data, Tref=in_par[-1])
    lin_trainer = fmu.LinearTrainer(mode="Gradient", bounds=bound, optimizer="L-BFGS-B")

    # Linear Training
    _ = lin_trainer.train(model=lin_model, loss_fun=lin_loss_function)

    
    # Post process results & print them to the console
    results = fmu.post_process(model=lin_model, train_data=lin_training_data, test_data=lin_test_data)
    print("\n-------------- Linear Training ---------------")
    lin_model.show_parameters()
    print("Train Loss: " + str(results[0]) + " -- Train-%: " + str(results[1]) + "%")
    print("Test Loss: " + str(results[2]) + " -- Test-%: " + str(results[3]) + "%")
    print("------------------------------------------------")
    exit()

    # ----------------------------------------------------------------------------------------------
    # Initialize Nonlinear Model
    # ----------------------------------------------------------------------------------------------

    # Create corresponding non-linear model with nlin paramter Beta=1
    nlin_model = NonLinearOscillators(model=nlin_case, lin_parameters=lin_model.parameters, nlin_parameters=1)

    # Create non-linear Trainer
    nlin_loss_function = build_loss_function(model=nlin_model, data=nlin_training_data, 
    Tref_Opt=False, Tref=lin_model.parameters[-1], nlin=True)
    nlin_trainer = NonLinearTrainer(mode="Gradient", optimizer="L-BFGS-B")

    # Non-Linear Training
    _ = nlin_trainer.train(model=nlin_model, loss_fun=nlin_loss_function, Tref=lin_model.parameters[-1])

    # Analyse results & print them to the console
    results = post_process(model=nlin_model, train_data=nlin_training_data, test_data=nlin_test_data)
    print("\n-------------- Non-Linear Training ---------------")
    nlin_model.show_parameters()
    print("Train Loss: " + str(results[0]) + " -- Train-%: " + str(results[1]) + "%")
    print("Test Loss: " + str(results[2]) + " -- Test-%: " + str(results[3]) + "%")
    print("------------------------------------------------")

    # Save results for post processing in .pkl format
    lin_model.save_model(Path(__file__).parent / '../output/saved_models/', save_name=None) 
    nlin_model.save_model(Path(__file__).parent / '../output/saved_models/', save_name=None) 

    # ----------------------------------------------------------------------------------------------
    # Plot Results (linear model only)
    # ----------------------------------------------------------------------------------------------
