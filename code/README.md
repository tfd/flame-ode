# Detailed Explanation of each Demo-File

## Frequency Domain Optimization

# Linear Models
We start by organizing the data at hand. The optimizer requires us to provide data as a single matrix with columns: gain, phase, weights and frequency. Each row in this matrix thus represents a measurement point defined by a frequency, reference gain, phase and corresponding weight. 

Weights allow us to assign more importance to certain measurement points. From practical experience, we recommend not using more than three weights with weight value $w>1$. We demonstrate how one can significantly improve results using weights in Sec. 4.1.2 of [Doehner et al. 2024](https://doi.org/10.1016/j.combustflame.2024.113408).

For the data at hand, we choose to assign more weight to the gain valleys and crest (and corresponding phase points).
```python
# Set weights
loss_weight = np.ones(len(ref_freq))
loss_weight[4] = 2
loss_weight[8] = 2
loss_weight[12] = 2
```

We make use of Python's *np.stack()* command to achieve the desired form.

```python
data=np.stack([ref_gain, ref_phase, loss_weight, ref_freq], axis=1)
```

With the data prepared, we can continue to define meta parameters for the frequency optimization routine. As explained in Sec. 3.2. of [Doehner et al. 2024]( https://doi.org/10.1016/j.combustflame.2024.113408), frequency domain optimization consists of two steps. In a first step, *LHS_probes* models are sampled using Latin Hypercube sampling (LHS). Since we can express the transfer function of linear models analytically, we can sample a large number of models, sort them by loss and retain a certain number of best-performing models to use their parameters as initial parameters for subsequent gradient-descent optimization. The number of models retained is governed by meta parameter *LBFGSB_PROBES*. 

```python
LHS_PROBES = 100000
LBFGSB_PROBES = 1000
```
The values chosen here should work well in most cases. The computationally demanding part of the overall routine is the gradient-descent optimization.

Both, LHS and gradient descent, require boundaries for each model parameter.

```python
opti_bound = np.array([[0.1, 0.1, 0.1, 0.001, 0.1, 0.1, 0.1, 0.1, 0.001],
[10, 10, 10, 10, 0.9, 10, 10, 10, 10]])
```

Values chosen here represent best practice values. The value range for each parameter can be extended, but larger value ranges typically implicate some deeper issues, such as e.g. a mismatch between model capacity and data complexity or poor time normalization. If one uses model 'sLDO', parameter ranges for the participation factor (see Eq. (7) of [Doehner et al. 2024]( https://doi.org/10.1016/j.combustflame.2024.113408)) must not exceed (0.1, 0.9) as otherwise, one LDO of the overall sLDO model is too passive (use model LDO instead then).

With our data and meta parameters set up, calling the actual frequency optimization routine is a simple line of code:
```python
res_params = freq_opti(data=data, model=MODEL, no_LHS=LHS_PROBES, no_LBFGS=LBFGSB_PROBES, TREF=1/1000)
```

The optimization routine returns a single set of parameters *res_params* which can be used in the next step to parametrize a flame model and access linear properties such as the unit impulse response or gain and phase. Currently, the frequency domain optimizer *freq_opti()* supports models 'LDO' and 'sLDO'. Regardless of which model is chosen, the delay is always modeled as explicit, i.e. via the $exp(-\tau \omega)$ term in the transfer function. Consequently, when using the results of frequency domain optimization to create a new flame model, the '_explicit' version of the corresponding model should be chosen. 
```python
MODEL = MODEL + '_explicit'
flameModel = LinearOscillators(model=MODEL, parameters=res_params)
```
To access properties such as bode diagram or impulse response we can simply call the corresponding model functions *.bode_plot()* and *.unit_impulse_response()*,
```python
sLDO_freq, sLDO_gain, sLDO_phase = flameModel.bode_plot(high_lim=ref_freq[-1])
sLDO_ir, sLDO_ir_time = flameModel.unit_impulse_response(duration=30)
```
where we use the keyword *high_lim* in *.bode_plot()* to set the upper-frequency limit for which gain and phase are calculated and the *duration* keyword in the *.unit_impulse_response()* call to set the time duration (in nondimensional time) for which the UIR is calculated. 

What remains is to visualize the gain, phase and impulse response of the model. For that task, we can use *matplotlib*. 

# Nonlinear Models
Optimization via the transfer function is computationally cheap to perform and yields good results. When switching to nonlinear flame models such as the NLDO or sNLDO, we can no longer formulate a transfer function. The only way to obtain information about gain and phase for a given frequency _and_ amplitude is to force a given flame model with a harmonic signal of such frequency and amplitude, record the output and apply a reverse FFT. For model NLDO, we can use the *.bode_plot()* function to do so (see *post_processing_alias.py* on the 'laminar' branch of the repository). This however is computationally very demanding. For model sNLDO, which comprises two NLDOs, the effort is even larger. However since the data points (defined by frequency and amplitude) can be computed independently of each other, we can parallelize the entire process to significantly improve runtime. This calculation is (as of now) not implemented inside the *LinearOscillator* class, but has to instead be called from another script, namely via *Parallalel_FDF_plot.py*. this script (currently) only supports model sNLDO.

At the top of said script, we can set model parameters, where we differentiate between linear parameters and nonlinear parameters. 

```python
# Set Model Parameters
lin_parameters = [0.36276682, 5.72186329, 0.66689266, 2.71285873, 0.52650767, 0.42986943,
                7.19032886, 1.04578806, 1.25259313, 1/1000]
nlin_parameters = [1, 1]
```

Linear parameters can be obtained from the previously described linear optimization routine. Nonlinear parameters need to be calibrated manually by plotting the predicted model FDF and comparing it to measurement data for different choices of $\beta_1$ and $\beta_2$. If no nonlinear reference data is available, setting $\beta_1=\beta_2=1$ is a good choice to introduce saturation effects observed in many flames.   

Next, we can define the amplitudes and frequencies we would like to investigate. For each amplitude, the script returns gain and phase for all frequencies defined by *eval_freq*. We also need to provide a time normalization factor *t_ref*.
```python
# Define Frequencies and Amplitudes of Interest
forcing=[0.1, 0.5, 1.0]
t_ref = 1/1000
eval_freq = np.arange(start=0, stop=501, step=10)
eval_freq[0] = 1
```

The script allows us to set two additional meta parameters. 
```python
no_of_dump_points=3 # No. of bode points calculated before temporary file is dumped
no_of_workers=8     # No. of parallel threads script will use
```
To limit the load on the RAM, the script dumps temporary results after a given number of data points into the *temp* folder in the output directory. Ideally, this parameter is set to be an integer multiple of the number of investigated amplitudes. If the script crashes reduce this parameter as a memory overflow is the most likely cause of any issues arising. The *no_of_worker* parameter defines the number of parallel threads the script uses. Most modern laptops should be able to support at least four threads. 

Lastly, we can define a custom save name used to save the results in the *files* folder within the output directory.
```python
save_name = 'sLDO_FDF_bode_results.h5'
```

The script saves the results as lists in an *.h5* file.
```python
 # Save data in h5-file
hf = h5py.File(dir_path / 'output/files' / save_name, 'w')
hf.create_dataset('gain', data=gain_list)
hf.create_dataset('phase', data=phase_list)
hf.create_dataset('freq', data=freq_list)
hf.create_dataset('forcing', data=forcing)
```
The gain and phase entries contain lists of gain and phase values for each forcing amplitude. For guidance on how to load and visualize the results, we refer to the *produce_figures.py* script on the 'turbulent' branch of this repository.
