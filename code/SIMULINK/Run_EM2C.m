% Script used to simulate the acoustics of a simple burner set-up in
% different configurations. The flame is modelled by model sNLDO with the
% parameter set-up for the Palies combustor as presented in Fig. 9 of the
% paper and Tab. S3 of the corresponding supplement. A more detailled
% description on the burner set-up and on how to connect model sNLDO to
% the corresponding acoustic simulation can be found in Sec. 7 of the
% supplement. 

% Author: Gregor Doehner & Camilo F. Silva, 2024
% Thermo-Fluid Dynamics Group
% TUM School of Engineering and Design
% Technical University of Munich

%% Standard header to clear workspace / output
clc
close all
clear

%% Set Model / Code Parameters

% Choose configurations to run
% cvec = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]; % All configs.
cvec = [4, 8, 12]; % Unstable ones only

%sim_time = 1.6; % Simulation time (s)
sim_time = 1200/1000; % Simulation time (s)
stop_time = 50/1000; % 50ms --> Time after which forcing is set to 0
dt = 1e-5; % Time Step
transient_time = 0.3;  % Transient time is excluded from post processing
Fmin = 130; % Minimum frequency of interest (in Hz)
Fmax = 150; % Maximum frequency of interest (in Hz)

% NO USER INPUT BEYOND THIS POINT REQUIRED
disp("Running Run_EM2C")

%% Defining Configurations of Interest
Conf{1} = [0.1248,0.1];  
Conf{2} = [0.1248,0.15]; 
Conf{3} = [0.1248,0.2];  
Conf{4} = [0.1248,0.4];  
Conf{5} = [0.1888,0.1];  
Conf{6} = [0.1888,0.15]; 
Conf{7} = [0.1888,0.2];  
Conf{8} = [0.1888,0.4];  
Conf{9} = [0.2528,0.1];  
Conf{10} =[0.2528,0.15]; 
Conf{11} =[0.2528,0.2];  
Conf{12} =[0.2528,0.4];  

%% Creating storage arrays for results
res_gains=zeros(1, length(Conf));
res_freq=zeros(1, length(Conf));
res_amp=zeros(1, length(Conf));

%% Creating White Noise
NyqF = 1/(2*dt);    % Nyquist frequency
Rat1 = Fmin/NyqF;
Rat2 = Fmax/NyqF;

N=ceil(sim_time/dt+1);
U = idinput(N,'RGS',[Rat1 Rat2])*1e-4;

% Set Forcing to Zero after user defined time
stop_idx = ceil(stop_time / dt);
U(stop_idx:end) = 0;

% Create timerseries obj.
time = (0:N-1)*dt;
inputSignal = timeseries(U, time);

%% Set Configuration of Test Rig --> Loop over diff. set-ups

for c = 1:length(cvec)

    c=cvec(c);
    
    Td=1600; % 1600, 1200, 800, 600, 500, 

    set_conf = Conf{c};     % Set specific configuration
    p=101325; Tu=300; gamma=1.4; R=287; 
    cu=sqrt(gamma*R*Tu); rho_u=p/(R*Tu);
    cd=sqrt(gamma*R*Td); rho_d=p/(R*Td);
    
    xiP = 1;
    xiC = rho_u*cu/(rho_d*cd);
    
    theta = Td/Tu-1;
    
    L1= set_conf(1); % 0.1248; % L=0.1248,L=0.1888, L=0.2528
    L2=0.1167;
    L3= set_conf(2); % 0.4;    % L=0.1, L=0.15, 0.2, L=0.4     
    
    A1=pi/4*(0.065)^2;
    A2=pi/4*(0.02117)^2;
    A3=pi/4*(0.070)^2;
    
    alphaP = A1/A2;
    alphaC = A2/A3;
    
    tau_P = L1/cu;
    tau_M = L2/cu;
    tau_C = L3/cd;
    
    DenP= xiP+alphaP;
    S11P = 2*xiP*alphaP/DenP;
    S12P = (xiP-alphaP)/DenP;
    S21P = (alphaP-xiP)/DenP;
    S22P = 2/DenP;
    
    DenC= xiC+alphaC;
    S11C = 2*xiC*alphaC/DenC;
    S12C = (xiC-alphaC)/DenC;
    S21C = (alphaC-xiC)/DenC;
    S22C = 2/DenC;
    
    H1 = alphaC*xiC*theta/DenC;
    H2 = alphaC*theta/DenC;
    
    % Load System
    model_name = 'EM2C_22';
    load_system(model_name);
    
    % Set parameters
    set_param([model_name,'/DelayPlenum1'], 'DelayTime',num2str(tau_P));
    set_param([model_name,'/DelayMidSec1'], 'DelayTime',num2str(tau_M));
    set_param([model_name,'/DelayCombChamb1'], 'DelayTime',num2str(tau_C));
    set_param([model_name,'/DelayPlenum2'], 'DelayTime',num2str(tau_P));
    set_param([model_name,'/DelayMidSec2'], 'DelayTime',num2str(tau_M));
    set_param([model_name,'/DelayCombChamb2'], 'DelayTime',num2str(tau_C));
    
    set_param([model_name,'/S11P'], 'Gain',num2str(S11P));
    set_param([model_name,'/S12P'], 'Gain',num2str(S12P));
    set_param([model_name,'/S21P'], 'Gain',num2str(S21P));
    set_param([model_name,'/S22P'], 'Gain',num2str(S22P));
    
    set_param([model_name,'/S11C'], 'Gain',num2str(S11C));
    set_param([model_name,'/S12C'], 'Gain',num2str(S12C));
    set_param([model_name,'/S21C'], 'Gain',num2str(S21C));
    set_param([model_name,'/S22C'], 'Gain',num2str(S22C));
    
    set_param([model_name,'/H1'], 'Gain',num2str(H1));
    set_param([model_name,'/H2'], 'Gain',num2str(H2));
    
    % Run the simulation
    simOut = sim(model_name, 'FixedStep', num2str(dt), 'StopTime', num2str(sim_time),'Solver', 'ode3');
    
    % Plot data for visual confirmation (one fig for each Config)
    figure(c)
    plot(simOut.tout,simOut.result.Data)
    dtsim = simOut.tout(2)-simOut.tout(1);
    FsampSim = 1/dtsim;
    trans_idx=ceil(transient_time/dt);
    y_out = simOut.result.Data(trans_idx:end);
    
    % Simple Max/Min Amplitude determination
    res_amp(c) = (max(y_out)-min(y_out))/2;

    % Additional FFT
    dt = simOut.tout(2)-simOut.tout(1);
    Fs = 1/dt;          % Sampling frequency (Hz)
    N = length(y_out);  % Number of samples
    
    % Perform FFT
    X = fft(y_out);
    
    % Compute the frequency axis
    f = Fs*(0:(N/2))/N;
    freq_spectrum=2/N * abs(X(1:N/2+1));
    [gain, idx] = max(freq_spectrum);
    res_gains(c)=gain;
    res_freq(c)=f(idx);
end

%% Create table to print to console
results = [1:1:length(Conf); res_gains; res_freq; res_amp];
results_table = table(results, 'VariableNames',{'Results for each Configuration'},'RowNames',{'Configuration', 'Gain','Frequency', 'Amplitude'});

%% Plotting


for c = 1:length(cvec)
    figure(cvec(c))

    fig = ['Palies_',num2str(c),'.pdf'];

    axis([0 1.2 -1.5 1.5])

    set(gca,'FontSize',16)
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperSize', [15 12]);
    set(gcf,'PaperPosition', [0 0 15 12])

    saveas(gcf,fig)

end

clc
disp("Finished Script - Check 'results_table' in workspace")