# Palies91

FDF data from Fig3 of [Silva et al. (2013)](https://doi.org/10.1016/j.combustflame.2013.03.020).

# FDF Palies91 Flame A
FDF data for flame A. Entries in h5 data structure are:

- gain: Contains gain data with structure [amplitudes x frequencies]
- phase: Contains phase data with structure [amplitudes x frequencies]
- frequency
- amplitude

