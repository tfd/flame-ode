# ETH Data

# ETH SI
Time series data with broadband input and corresponding output.
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response of CFD
- Ts: Time step used for CFD
- impulse_time: Time vector for reference impulse response of FIR model
- impulse_response: Reference impulse response of FIR model
- gain: Bode gain of FIR model
- phase: Bode phase of FIR model 
- frequency: Frequency vector on which bode gain/phase are calculated
- exp_gain: Bode gain of experiment
- exp_gain_frequency: Frequency vector on which bode gain was determined in experiment
- exp_phase: Bode phase of experiment
- exp_phase_frequency: Frequency vector on which bode phase was determined in experiment


# ETH SI FIR prediction
Time series data with broadband input and corresponding output produced by FIR model.
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response of FIR model
- Ts: Time step used for FIR model (equalt to CFD)

# ETH_Alexander Eder_10_Amp
Time series data with broadband input and corresponding output. Produced with forcing amplitudes of 10. 
Corresponds to ETH SI file but contains much longer signals. 
File contains following entries:
## Broadband Data
- U: Broadband input forcing signal
- Q: Resulting response of CFD
- Ts: Time step used for CFD
- time: Corresponding time vector of forcing


# ETH_Alexander Eder_50_Amp
Time series data with broadband input and corresponding output. Produced with forcing amplitudes of 50. 
Corresponds to ETH SI file but contains much longer signals. 
File contains following entries:
## Broadband Data
- U: Broadband input forcing signal
- Q: Resulting response of CFD
- Ts: Time step used for CFD
- time: Corresponding time vector of forcing


# ETH_Alexander Eder_100_Amp
Time series data with broadband input and corresponding output. Produced with forcing amplitudes of 100. 
Corresponds to ETH SI file but contains much longer signals. 
File contains following entries:
## Broadband Data
- U: Broadband input forcing signal
- Q: Resulting response of CFD
- Ts: Time step used for CFD
- time: Corresponding time vector of forcing


# ETH_LES_440ms_10_Amp
Time series data with broadband input and corresponding output. Produced with forcing amplitudes of 10.
Extra long signal containing 29.3IRs 
- U: Broadband input forcing signal
- Q: Resulting response of CFD
- Ts: Time step used for CFD

# ETH experimental data
Data containing experimental gain and phase values and corresponding frequencies at which they occurred. Impulse response data was obtained via post-processing from experimental gain and phase data. In order to obtain stable results for UIR artificial gain and phase data points were added for frequencies [0, 10, 20, 30, 40] (Hz). Gain and phase values for these frequencies are not stored in the corresponding gain, phase and frequency fields, as they contain no true experimental data. Serves as reference FTF data.

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse response obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to the impulse response

# ETH experimental rational fit data
Data containing EXPERIMENTAL gain and phase values and corresponding frequencies at which they occured. Impulse response data was obtained via post processing routine that applied a rational fit to experimental gain and phase data and converts the resulting fitted FTF into an UIR.

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse reponse obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to impulse response

# ETH experimental FDF data
Data containing TODO
- amp
- freq
- mean_gain
- up_gain
- low_gain
- mean_phase
- up_phase
- low_phase
