# BRS Data

# BRS incompressible
Time series data with broadband input and corresponding output.
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response from CFD
- Ts: Time step of CFD simulation
- impulse_time: Time vector for SI impulse response
- impulse_response: SI impulse response
- gain: Bode gain of SI model
- phase: Bode phase of SI model 
- frequency: Frequency vector on which bode gain/phase are calculated for SI model
- exp_gain: Bode gain of experiment
- exp_phase: Bode phase of experiment
- exp_frequency: Frequency vector on which bode gain/phase were determined in experiment

# BRS incompressible FIR prediction
Time series data with broadband input and corresponding output.
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response of FIR model
- Ts: Time step used for FIR model (equalt to CFD)

# BRS experimental data
Data containing EXPERIMENTAL gain and phase values and corresponding frequencies at which they occured. Impulse response data was obtained via post processing from experimental gain and phase data!

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse reponse obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to impulse response

# BRS experimental rational fit data
Data containing EXPERIMENTAL gain and phase values and corresponding frequencies at which they occured. Impulse response data was obtained via post processing routine that applied a rational fit to
experimental gain and phase data and converts the resulting fitted FTF into an UIR.

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse reponse obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to impulse response