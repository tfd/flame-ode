# Synthetic Data
Data created with Oscillators and broadband input taken from a reference data set
 
## SyntheticTurbulentData_06_2022.h5
Data created to investigate whether explicit time delay models can recover the same effects as their Padé pendants
- Model: superimposed_mod2_LDO
- Model Parameters: 0.945324060,  3.90483469,  2.52063928,  0.1, -0.683937180, 0.945324060,
  0.207358808, 0.136033904, 1.5, 1.02741979e-03
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 10.06.2022#
- Undersampling: 1
- Notes: Ref. parameter originate from a superimposed mod2 LDO system that was optimized on KulkaGuo21 gain directly via the TF
- Notes: Data was created with the explicit Euler scheme

## ExpImposedLDO_turbData1.h5
!!! File contains potentially wrong data as a bug in explicit models superimposed position of wrong mass!!!
Data created to investigate whether explicit models can recover a given parametrization of themselves
- Model: superimposed_mod2_LDO_explicit
- Model Parameters: 0.254, 0.7515, 0.1078, 0.122, 0.7681, 1.4044, 1.8509, 3.1799, 3.0, 0.0008
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 15.06.2022
- Undersampling: 1
- Notes: Ref. parameter originate from an explicit superimposed mod2 LDO system optimized on Sagar turbulent data (best model of study3)
- Notes: Data was created with the explicit Euler scheme

## ExpImposedLDO_turbData2.h5
!!! File contains potentially wrong data as a bug in explicit models superimposed position of wrong mass!!!
Data created to investigate whether explicit models can recover a given parametrization of themselves
- Model: superimposed_mod2_LDO_explicit
- Model Parameters: 0.254, 0.7515, 0.1078, 0.122, 0.7681, 1.4044, 1.8509, 3.1799, 3.0, 0.0008
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 15.06.2022
- Undersampling: 1
- Notes: Ref. parameter originate from an explicit superimposed mod2 LDO system optimized on Sagar turbulent data (best model of study3)
- Notes: Data was created with the explicit Euler scheme

## ExpImposedLDO_turbData3.h5
!!! File contains potentially wrong data as a bug in explicit models superimposed position of wrong mass!!!
Data created to investigate whether explicit models can recover a given parametrization of themselves
- Model: superimposed_mod2_LDO_explicit
- Model Parameters: 0.254, 0.7515, 0.1078, 0.122, 0.7681, 1.4044, 1.8509, 3.1799, 3.0, 0.0008
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 15.06.2022
- Undersampling: 1
- Notes: Ref. parameter originate from an explicit superimposed mod2 LDO system optimized on Sagar turbulent data (best model of study3)
- Notes: Data was created with the explicit Euler scheme

## SynthLDO_turbData1.h5
Data created to compare different numerical schemes to each other (using the base LDO model for simplicity)
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 03.07.2022
- Undersampling: 1
- Notes: Data was created with the explicit Euler scheme

## SynthLDO_turbData2.h5
Data created to compare different numerical schemes to each other (using the base LDO model for simplicity)
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 03.07.2022
- Undersampling: 1
- Notes: Data was created with the Trapezoidal scheme

## SynthLDO_turbData2.h5
Data created to compare different numerical schemes to each other (using the base LDO model for simplicity)
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34
- Input Data: '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Date of Creation: 03.07.2022
- Undersampling: 1
- Notes: Data was created with the RK4 scheme

## SolverStudy-Data
The data is used for a uniform and standardized ivestigation of solver performance. Pade and explicit super imposed models 
try to recover their own parametrisation with an ever increasing number of LHS samples. The same target parametrization (in terms
of actual parameters) is used, sinnce a short study on "zeta --> delay" relation, revealed that the assumption of qualitatively similar
behaving explicit and pade delays is appropriate for the parameters at hand.

!!!! Both data files saved potentially wrong IRs as there's a bug somewhere in the IR code!!!!
This is not relevant for the study at hand 

### SolverStudy_ExpTargetData.h5
- Model: superimposed_mod2_LDO_explicit
- Model Parameters: 0.945324060, 3.90483469, 2.52063928, 0.1, -0.683937180, 0.945324060, 0.207358808, 0.136033904, 1.5, 1.02741979e-03
- Input Data = '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Integrator='RK4'
- Undersampling: 1

### SolverStudy_PadeTargetData.h5
- Model: superimposed_mod2_LDO
- Model Parameters: 0.945324060, 3.90483469, 2.52063928, 0.1, -0.683937180, 0.945324060, 0.207358808, 0.136033904, 1.5, 1.02741979e-03
- Input Data = '../data/KulkaGuo21/KulkaGuo21_823K.h5'
- Integrator='RK4'
- Undersampling: 1

### DoehnHaeri22_LDO_SilvaEmmer15_Data_Euler.h5
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34 (DoehnHaeri22 Config)
- Input Data: '../data/SilvaEmmer15_DNS_synthetic_linear.h5'
- Integrator: Explicit Euler
- Undersampling: 1
- Notes: Used for simple Neural ODE demonstration

## Neural ODEs

### DoehnHaeri22_LDO_SilvaEmmer15_Data_Euler_Usamp_100.h5
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34 (DoehnHaeri22 Config)
- Input Data: '../data/SilvaEmmer15_DNS_synthetic_linear.h5'
- Integrator: Explicit Euler
- Undersampling: 15
- Notes: Used for simple Neural ODE demonstration

### DoehnHaeri22_LDO_SilvaEmmer15_Data.h5
- Model: LDO
- Model Parameters: 0.9393, 0.8698, 0.6091, 1/833.34 (DoehnHaeri22 Config)
- Input Data: '../data/SilvaEmmer15_DNS_synthetic_linear.h5'
- Integrator: Trapezoidal
- Undersampling: 1
- Notes: Used for simple Neural ODE demonstration (with implicit integrator)

### DoehnHaeri22_NLDO_JaensPolif17_DNS_non_linear_Usamp_15.h5
- Model: NLDO
- Model Parameters: 0.9393, 1.2347, 0.8698, 0.6091, 1/833.34 (DoehnHaeri22 Config)
- Input Data: '../data/JaensPolif17_DNS_non_linear.h5'
- Integrator: Explicit Euler
- Undersampling: 15
- Notes: Used for simple Neural ODE demonstration (with implicit integrator)