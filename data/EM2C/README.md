# KulkaGuo21 Data

## KulkaGuo21_823K
Time series data from KulkaGuo21 paper -- broadband input and corresponding output. Turbulent data WITH combustion noise

## KulkaGuo21_823K_clean
Time series data with broadband input and corresponding output. Turbulent data BUT WITHOUT combustion noise. Data is produced by taking the fully trained Sagar Model, deactivating its noise modelling contributions and simulate the response to some braodband input. 
File contains following entries:
- U: Broadband input forcing signal
- Q: Resulting response of clean FIR model
- Ts: Time step used for FIR model 
- impulse_time: Time vector of model IR
- impulse_response: Model IR
- gain: Bode gain of FIR model
- phase: Bode phase of FIR model 
- frequency: Frequency vector on which bode gain/phase are calculated

# KulkaGuo21 823K FIR prediction
Time series data with broadband input and corresponding output.
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response of FIR model
- Ts: Time step used for FIR model (equalt to CFD)

# KulkaGuo21 823K FIR prediction reference
Time series data with broadband input and corresponding output. Same information
as in default reference file, but with much higher undersampling as chosen per
default by MATLAB during creation of FIR model. Used only for comparison to literature!
File contains following entries:

- U: Broadband input forcing signal
- Q: Resulting response of FIR model
- Ts: Time step used for FIR model (equalt to CFD)

# KulkaGuo21 823K experimental data
Data containing EXPERIMENTAL gain and phase values and corresponding frequencies at which they occured. Impulse response data was obtained via post processing from experimental gain and phase data!

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse reponse obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to impulse response

# KulkaGuo21 823K experimental rational fit data
Data containing EXPERIMENTAL gain and phase values and corresponding frequencies at which they occured. Impulse response data was obtained via post processing routine that applied a rational fit to
experimental gain and phase data and converts the resulting fitted FTF into an UIR.

- exp_gain: Gain measured from experiment
- exp_phase: Phase measured from experiment
- exp_freq: Frequencies for which experiment was performed
- exp_impulse_response: Impulse reponse obtained from experimental FTF data
- exp_impulse_time: Time vector corresponding to impulse response
