# Database 
Time series data, impulse responses, gain and phase data that can be used to train models and analyse them in post processing. Data can be loaded via the following steps:

1. Load the file of interest
```python
h5file = h5py.File(Path(__file__).parent / '../data/Kornilov_FTF.h5', 'r')
```

2. Display all keys (kind of "folders") the file contains
```python
h5file.keys()
```

3. Load the keys of interest into your program
```python
r_frequency = np.array(h5file.get('frequency'))
r_gain = np.array(h5file.get('gain'))
r_phase = np.array(h5file.get('phase'))
```

4. Close the file you were loading
```python
h5file.close()
```

## MH
Data created by M. Haeringer 2021 based on set-up of [Jaensch, Polifke, 2017](https://www.researchgate.net/publication/316978486_Uncertainty_encountered_when_modelling_self-excited_thermoacoustic_oscillations_with_artificial_neural_networks) and used as test data for G. Doehner et al. . 
- Haeringer_Comp: Compressible simulation 
- Haeringer_CompFix: Compressible simulation with modified BC to maintain character of flame
- Haeringer_Incomp: Incompressible simulation

Incompressible simultation is similar to [Jaensch, Polifke, 2017](https://www.researchgate.net/publication/316978486_Uncertainty_encountered_when_modelling_self-excited_thermoacoustic_oscillations_with_artificial_neural_networks) but incompressible simulation converges slow (in OpenFOAM). Amplitude in compressible case differs due to corrupted input signal (affected by flame feedback). Case "CompFix" represents a compressible simulation but the velocity (input forcing) is directly imposed as B.C. leading to roughly the same behaviour as in the incompressible case.  

File _Haeringer_xFDF.h5_ contains the extended FDF (xFDF) as introduced in [Haeringer, 2019](https://doi.org/10.1016/j.proci.2018.06.150). The file contains the following entries.

**Gain:** 
- gain/0.025
- gain/0.25
- gain/0.5
- gain/1.0
- gain/1.5

**Phase:** 
- phase/0.025
- phase/0.25
- phase/0.5
- phase/1.0
- phase/1.5

Where as each entry contains an array with two columns. The first contains the frequencies for which the corresponding gain or phase are available. The second column contains the actual data, i.e. gain or phase for the corresponding amplitude of [0.025, 0.25, 0.5, 1.0, 1.5], depending on the file which was loaded. See below for an example on how to access the data.

```python
h5file = h5py.File(Path(__file__).parent / '../data/Haeringer_xFDF.h5', 'r')
# Load gain data for amplitude A=0.025
gain_data_0_025 = np.array(h5file.get('gain/0.025'))
# Extract frequency and gain
freq_0_025 = gain_data_0_025[0,:]
gain_0_025 = gain_data_0_025[1,:]
``` 

## JaensPolif17
Data taken from [Jaensch, Polifke, 2017](https://www.researchgate.net/publication/316978486_Uncertainty_encountered_when_modelling_self-excited_thermoacoustic_oscillations_with_artificial_neural_networks)

- JaensPolif17_DNS_linear
- JaensPolif17_DNS_non_linear
- JaensPolif17_DNS_partly_non_linear

## Kornilov
Data taken from [Kornilov et al., 2009](https://www.researchgate.net/publication/5326700_Experimental_and_Numerical_Investigation_of_the_Acoustic_Response_of_Multi-slit_Bunsen_Burners)
- Kornilov_FDF
- Kornilov_FTF
- Kornilov_Gain
- Kornilov_Phase

## SilvaEmmer15
Data taken from [Silva et al., 2015](https://www.researchgate.net/publication/271326115_Numerical_study_on_intrinsic_thermoacoustic_instability_of_a_laminar_premixed_flame) 
- SilvaEmmer15_DNS_synthetic_linear