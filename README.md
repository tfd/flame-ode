# How2FlameODE - A Practitioners Guide to Flame Modelling

This repository contains the _flameODE_ package as well as all scripts that were used to create results presented in
- [Doehner et al. 2022]( https://doi.org/10.1177/17568277221094760) (laminar flames)
- [Doehner et al. 2024](http://dx.doi.org/10.1016/j.combustflame.2024.113408) (turbulent flames)

For a detailed discussion of each work and corresponding code base, we refer to either the _laminar_ or _turbulent_ branch of this repository. This main branch serves as an overview and introduction to the _flameODE_ package. 

Our proposed flame model can be trained using time domain or frequency reference data. In the *code* folder, we provide two scripts demonstrating how to do so depending on the data available. 

In addition, we describe the conceptual approach in detail below.

**Note:** Currently, the 'laminar' and the 'turbulent' branches of this repository rely on different versions of the _flameODE_ package. We recommend using the (newer) version made available on this _main_ as well as the _turbulent_ branch. 

## Step 1: Set-Up Data
Depending on whether you have access to frequency or time domain reference data, said data has to be provided in different ways. If you have access to both, we suggest first calibrating the model using the frequency domain approach and using the resulting parametrization as an initial guess for a subsequent time domain optimization approach, refining the model.

### Frequency Domain 
If measurements in the frequency domain are available, we can easily bring them to the correct form using NumPy's *np.stack()* command. For calibration, we require reference gain and phase, the corresponding frequency points (in Hz) and a vector of loss weights.
```python
import numpy as np
data=np.stack([ref_gain, ref_phase, loss_weight, ref_freq], axis=1)
```
The vector of loss weights allows us to assign more importance to certain measurement points, facilitating calibration to more complex transfer functions (see Sec. 3.2 and 4.1.2 in [Doehner et al. 2024](http://dx.doi.org/10.1016/j.combustflame.2024.113408) for more details).

### Time Domain
In the case of time domain calibration, we rely on time series data of broadband forcing of the target flame. To handle data efficiently, we make use of the *DataLoader* class of the flameODE package. 

```python
from pathlib import Path
from flameODE_utility.general_util import DataLoader
dir_path = Path(__file__).parent.parent.resolve()
TRAIN_DATA_PATH = dir_path / 'data/ETH/ETH_LES_440ms_10_Amp.h5'
dataloader = DataLoader(train_path, test_path=None, u_sample_train=15, ign_val=0.015)
```

For this example, we do not provide any test data to the *DataLoader*. For a demonstration of how to use test data with the *DataLoader*, check out the *time_domain_demo.py* demonstration code in the *code* folder on this branch. To access the training data, we call the newly created *DataLoader* object
```python
train_data = dataloader.return_training_data(ir_multiples=6.5)
```
loading reference data with 6.5 times the length of the impulse response of the flame. The *train_data* dictionary is required as input to time domain optimizers.

## Step 2: Which Model to Use?
Depending on the complexity of the flame at hand, different models are recommended to use. To differentiate flame dynamics in terms of their complexity we rely on the terminology introduced in Sec. 4.1.1 of [Doehner et al. 2024](http://dx.doi.org/10.1016/j.combustflame.2024.113408). *Low capacity* systems consider the system’s response to one perturbation class. Physically, such a system may be understood as the impulse response of a flame to incoming axial velocity perturbations characterized by one crest and one valley. The IR crest (valley) describes the state of maximum (minimum) heat release, i.e., which can be associated with the state of maximum (minimum) elongation if a premixed flame is considered. Such systems comprise simple gain dynamics with one distinct peak in gain, unity low-frequency limit and low-pass filter behavior.

![low_capacity](../output/figures/LC_Gain_demo.png)

*High capacity* systems encompass a system’s response to two perturbations. In addition to the response to axial acoustic perturbations, as modeled by the *low capacity* system, the *high capacity* system considers the response to another perturbation class, which may travel at a different speed. Physically, this second perturbation can be associated with tangential fluctuations produced at the trailing edge of typical axial swirler blades when subjected to upstream axial flow perturbations. Tangential fluctuations convect approximately with the mean flow speed before reaching the flame base. As a consequence of the two aforementioned perturbation classes (axial and tangential) traveling at different speeds, several maxima in the gain of the frequency response can be observed, which is a common feature in the response of swirl-stabilized flames.

![high_capacity](../output/figures/HC_Gain_demo.png)

## Step 3: Linear Model Calibration

### Frequency Domain 
With our data assembled as described in Step 1, we can now call the transfer function optimization utility of the flameODE package by simply typing
```python
from flameODE_utility.freq_opt import freq_opti
MODEL='sLDO' # or 'LDO'
res_params = freq_opti(data=data, model=MODEL, no_LHS=100000, no_LBFGS=1000, TREF=1/1000)
```
where *model* defines the model used for optimization, *no_LHS* sets the number of samples drawn via Latin Hypercube sampling and *no_LBGS* defines the number of samples that are further optimized using gradient descent. *TREF* represents the time normalization factor as introduced in Eq.(16) of [Doehner et al. 2022]( https://doi.org/10.1177/17568277221094760). A more detailed description of the frequency domain optimization method can be found in Sec. 3.2 of [Doehner et al. 2024](http://dx.doi.org/10.1016/j.combustflame.2024.113408). The optimizer returns a set of parameters (without *TREF*) which can then be used to initialize a fully calibrated model of *model* type.

```python
from flameODE_utility.model_util import LinearOscillators
TREF=1/1000
PAR=np.concatenate([res_params, [TREF]])
flame_model = LinearOscillators(model='sLDO_duct', parameters=PAR)
```

Note that models *model='LDO'* and *model='sLDO'* as implemented in the *freq_opti* function use an explicit time delay in their transfer function. Consequently, we need to initialize the corresponding flame model with either a duct (*model=LDO_duct* / *model=sLDO_duct*) or an explicit time delay (*model=LDO_explicit* / *model=sLDO_explicit*).

### Time Domain
Regardless of the model and optimization method, during time domain optimization we aim at minimizing the loss
$$ \mathcal{J} = |y_{model}-y_{ref}|_2$$
with model prediction $y_{model}$ and reference CFD data $y_{ref}$. Depending now on the model and the level of noise in the data set, different optimization strategies can be used to achieve a minimization of $\mathcal{J}$. 

When using the low capacity model *model=LDO*, we can build a trainer object that uses gradient descent to minimize $\mathcal{J}$. To do so, we first initialize a model with an initial parameter guess, typically a uniform parametrization.
```python
from flameODE_utility.model_util import build_loss_function
in_par=[1,1,1,1,1/1000]
lin_model = LinearOscillators(model="LDO", parameters=in_par)
```
With our model at hand, we can proceed to define our loss function:
```python
from flameODE_utility.model_util import build_loss_function
lin_loss_function = build_loss_function(model=lin_model, data=train_data, Tref=in_par[-1])
```
Next, we initialize a trainer object:
```python
from flameODE_utility.model_util import LinearTrainer
bound = np.array([[0.1, 0.1, 0.1, 0.01], [5, 5, 5, 1.5]])
lin_trainer = LinearTrainer(mode="Gradient", bounds=bound, optimizer="L-BFGS-B")
```
where we also need to provide some lower and upper boundaries for the model parameters except for *TREF*, as the time normalization factor is not optimized but assumed a priori. 

We now have everything assembled to train the model by simply typing
```python
_ = lin_trainer.train(model=lin_model, loss_fun=lin_loss_function, eps_constraint=True)
```
The calibrated parameters are automatically stored in *lin_model*, thus we do not need any return value of *lin_trainer.train()*.

The above-described training method can be used with all models of class *LinearOscillator*, including the high capacity model *sLDO*. However, as described in [Doehner et al. 2024](http://dx.doi.org/10.1016/j.combustflame.2024.113408), direct optimization of loss $\mathcal{J}$ may not always be possible. In the case of complex flame dynamics and noise (turbulent flames), we need to use the more robust, but computationally significantly more demanding, *switch optimization* technique. A detailed description of this method can be found in Section S4 of the [Supplement of Doehner et al. 2024](https://ars.els-cdn.com/content/image/1-s2.0-S0010218024001172-mmc1.pdf). For a demonstration of how to use this method, we refer to file *time_domain_optimization.py* on branch 'turbulent_flames'.

## Step 4: Nonlinear Model Calibration
Nonlinear model optimization is challenging. Frequency domain optimization is no longer a valid option as the transfer function cannot be expressed analytically.

Currently, the flameODE package supports gradient-descent optimization using time domain data only for model 'NLDO'. For model 'sNLDO' we recommend either setting both nonlinear saturation parameters $\beta_1$ and $\beta_2$ to one or performing manual calibration of $\beta_i$ by plotting the FDF for multiple different values of $\beta_i$. We explain this approach in more detail in the *code* folder on this main branch. 

Below, we demonstrate how to further optimize the model 'NLDO' building on top of the parameters obtained from linear optimization and using time domain training data. We start by initializing our nonlinear model, the 'NLDO'.
```python
nlin_model = NonLinearOscillators(model=nlin_case, lin_parameters=lin_model.parameters, nlin_parameters=1)
```
Again, as in the linear case, we need to define our loss function,
```python
nlin_loss_function = build_loss_function(model=nlin_model, data=nlin_training_data, Tref=lin_model.parameters[-1], nlin=True)
```
initialize our NonLinearTrainer object:
```python
from flameODE_utility.model_util import NonLinearTrainer
nlin_trainer = NonLinearTrainer(mode="Gradient", optimizer="L-BFGS-B")
```
And ultimately train the nonlinear model using the *.train()* function of the nonlinear trainer object.
```python
_ = nlin_trainer.train(model=nlin_model, loss_fun=nlin_loss_function, Tref=lin_model.parameters[-1])
```

## Step 5: Post Processing
As a result of any calibration routine, you obtain either a set of parameters defining a given flame model or already the model itself. Linear properties such as the unit impulse response or the corresponding bode diagram can easily be accessed using the utilities of the flame model. 

Let's consider for example the bode diagram of a calibrated model, 'flame_model.
```python
m_freq, m_gain, m_phase = flame_model.bode_plot()
```
What is left is to plot the obtained data and format the figures accordingly.

```python
from matplotlib import pyplot as plt
import matplotlib

# Gain
plt.figure()
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(6, 3)
plt.plot(m_freq, m_gain, '#1E90FF')
plt.legend()
plt.ylabel('Gain')
plt.xlabel('Frequency (Hz)')
plt.tight_layout()
plt.show()

# Phase
plt.figure()
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(6, 3)
plt.plot(m_freq, m_phase, '#1E90FF')
plt.legend()
plt.ylabel('Phase (rad)')
plt.xlabel('Frequency (Hz)')
plt.tight_layout()
plt.show()
```

Plotting the impulse response is equally simple:

```python
m_ir, m_ir_time = flame_model.unit_impulse_response()

# IR
plt.figure()
plt.plot(m_ir_time, m_ir, '#1E90FF')
plt.legend()
plt.ylabel('Amplitude')
plt.xlabel('Physical Time (s)')
plt.tight_layout()
plt.show()
```

## Step 6: Beyond _flameODE_ - Exporting a Model to MATLAB
